package org.cpe.tennismaster.court.controller;

import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.common.roles.Roles;
import org.cpe.tennismaster.court.common.dto.CourtDto;
import org.cpe.tennismaster.court.common.vue.CourtLibelleVue;
import org.cpe.tennismaster.court.common.vue.CourtVue;
import org.cpe.tennismaster.court.service.CourtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/court")
public class CourtController {

    private final CourtService courtService;

    @Autowired
    public CourtController(CourtService courtService) {
        this.courtService = courtService;
    }

    @GetMapping
    public ResponseEntity<List<CourtVue>> getAllCourts() {
        return ResponseEntity.ok(courtService.getAllCourts());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CourtVue> getCourtById(@PathVariable Integer id) throws ParameterException {
        return ResponseEntity.ok(courtService.getCourtById(id));
    }

    @GetMapping(value = "/byCKAndCover")
    @PreAuthorize("hasAnyRole('"+ Roles.Libelle.USER + "','"+Roles.Libelle.ADMIN + "')")
    public ResponseEntity<List<CourtVue>> getCourtByCourtKindAndIsCover(@RequestParam(required = false) Boolean isCovered,
                                                                        @RequestParam(required = false) Integer idCourtType,
                                                                        @RequestParam(required = false) String libelleCourt,
                                                                        @RequestParam(required = false) Integer idCourt) throws ParameterException {
        return ResponseEntity.ok(courtService.getCourtByCourtKindAndIsCover(isCovered, idCourtType, libelleCourt, idCourt));
    }

    @PostMapping(value = "/libelles")
    public ResponseEntity<List<CourtLibelleVue>> getAllCourtsLibelle() throws NotFoundException {
        return ResponseEntity.ok(courtService.getAllCourtsLibelle());
    }

    @PostMapping
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<CourtVue> createCourt(@RequestBody CourtDto courtDto) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.CREATED).body(courtService.createCourt(courtDto));
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<Void> deleteCourt(@PathVariable Integer id) throws NotFoundException {
        return ResponseEntity.ok(courtService.deleteCourt(id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<CourtVue> updateCourt(@PathVariable Integer id, @RequestBody CourtDto courtDto) throws NotFoundException {
        return ResponseEntity.ok(courtService.updateCourt(id, courtDto));
    }

}
