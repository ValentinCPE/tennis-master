package org.cpe.tennismaster.court.service;

import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.court.common.dto.CourtDto;
import org.cpe.tennismaster.court.common.vue.CourtLibelleVue;
import org.cpe.tennismaster.court.common.vue.CourtVue;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CourtService {

    List<CourtVue> getAllCourts();

    CourtVue getCourtById(Integer id) throws ParameterException;

    List<CourtVue> getCourtByCourtKindAndIsCover(Boolean isCovered, Integer idCourtType, String libelleCourt, Integer idCourt) throws ParameterException;

    CourtVue createCourt(CourtDto courtDto) throws NotFoundException;

    List<CourtLibelleVue> getAllCourtsLibelle() throws NotFoundException;

    Void deleteCourt(Integer id) throws NotFoundException;

    CourtVue updateCourt(Integer id, CourtDto courtDto) throws NotFoundException;

    /*Court updateCourt(Court court);*/
}
