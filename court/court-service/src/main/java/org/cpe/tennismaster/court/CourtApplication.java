package org.cpe.tennismaster.court;

import org.cpe.tennismaster.common.CommonGeneralApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@Import(CommonGeneralApplication.class)
@SpringBootApplication
public class CourtApplication {

    public static void main(String[] args) {
        SpringApplication.run(CourtApplication.class, args);
    }

}
