package org.cpe.tennismaster.court.service.impl;

import com.sun.jndi.url.corbaname.corbanameURLContextFactory;
import info.debatty.java.stringsimilarity.NormalizedLevenshtein;
import lombok.extern.slf4j.Slf4j;
import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.common.utils.UtilsTools;
import org.cpe.tennismaster.court.common.dto.CourtDto;
import org.cpe.tennismaster.court.common.vue.CourtLibelleVue;
import org.cpe.tennismaster.court.common.vue.CourtVue;
import org.cpe.tennismaster.court.model.Court;
import org.cpe.tennismaster.court.repository.CourtRepository;
import org.cpe.tennismaster.court.service.CourtService;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeToken;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Slf4j
@Service
public class CourtServiceImpl implements CourtService {

    private final CourtRepository courtRepository;

    private ModelMapper modelMapper;

    @Autowired
    public CourtServiceImpl(CourtRepository courtRepository, ModelMapper modelMapper) {
        this.courtRepository = courtRepository;
        this.modelMapper = modelMapper;
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }

    public List<CourtVue> getAllCourts() {
        Optional<List<Court>> courtList =  this.courtRepository.findAllBy();
        if(courtList.isPresent()) {
            Type listType = new TypeToken<List<CourtVue>>() {}.getType();
            return modelMapper.map(courtList.get(), listType);
        }
        return Collections.<CourtVue>emptyList();
    }

    public CourtVue getCourtById(Integer id) throws ParameterException {
        Optional<Court> optCourt = courtRepository.findById(id);
        if (!optCourt.isPresent()) {
            throw new ParameterException();
        }
        return modelMapper.map(optCourt.get(), CourtVue.class);
    }


    public List<CourtVue> getCourtByCourtKindAndIsCover(Boolean isCovered, Integer idCourtType, String libelleCourt, Integer idCourt) throws ParameterException {
        Optional<List<Court>> courtListFromCriteria = Optional.empty();
        if (libelleCourt != null || idCourt != null) {
            if(idCourt != null) {
                courtListFromCriteria = courtRepository.findByIdCourt(idCourt);
            } else {
                try {
                    courtListFromCriteria = courtRepository.findByLibelleCourt(URLDecoder.decode(libelleCourt, StandardCharsets.UTF_8.toString()));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        } else {
            if (isCovered != null && idCourtType != null) {
                courtListFromCriteria = courtRepository.findAllIdsByIsCoveredAndCourtKindId(isCovered, idCourtType);
            } else if (isCovered != null) {
                courtListFromCriteria = courtRepository.findAllIdsByIsCovered(isCovered);
            } else if (idCourtType != null) {
                courtListFromCriteria = courtRepository.findAllIdsByCourtKindId(idCourtType);
            } else {
                courtListFromCriteria = courtRepository.findAllBy();
            }
        }
        if (courtListFromCriteria.isPresent()) {
            Type listType = new TypeToken<List<CourtVue>>() {}.getType();
            return modelMapper.map(courtListFromCriteria.get(), listType);
        }
        return Collections.<CourtVue>emptyList();
    }

    public CourtVue createCourt(CourtDto courtDto) throws NotFoundException {
        if (courtDto == null) {
            throw new NotFoundException("Court");
        }
        Court courtModel = modelMapper.map(courtDto, Court.class);
        courtModel = courtRepository.save(courtModel);
        return modelMapper.map(courtModel, CourtVue.class);
    }

    public List<CourtLibelleVue> getAllCourtsLibelle() throws NotFoundException {
        Optional<List<Court>> courtList = courtRepository.findAllBy();
        if(courtList.isPresent()) {
            Type listType = new TypeToken<List<CourtLibelleVue>>() {}.getType();
            return modelMapper.map(courtList.get(), listType);
        }
        return Collections.<CourtLibelleVue>emptyList();
    }

    public Void deleteCourt(Integer id) throws NotFoundException {
        Optional<Court> optCourt = courtRepository.findById(id);
        if (!optCourt.isPresent()) {
            throw new NotFoundException("Court");
        }
        courtRepository.delete(optCourt.get());
        return null;
    }

    public CourtVue updateCourt(Integer id, CourtDto courtDto) throws NotFoundException {
        if (courtDto == null) {
            throw new NotFoundException("Court");
        }
        Optional<Court> optExistingCourt = courtRepository.findById(id);
        if (!optExistingCourt.isPresent()) {
            throw new NotFoundException("Court");
        }
        Court courtModel = modelMapper.map(courtDto, Court.class);
        UtilsTools.copyNonNullProperties(courtModel, optExistingCourt.get());
        Court courtModelUpdated = courtRepository.save(optExistingCourt.get());

        if (courtModelUpdated == null) {
            throw new NotFoundException("Court");
        }
        return modelMapper.map(courtModelUpdated, CourtVue.class);
    }

    /*public Court updateCourt(Court court) {
        if (court == null) {
            throw new BadRequestException();
        }
        Optional<Court> optExistingCourt = courtDao.findById(court.getId());
        if (!optExistingCourt.isPresent()) {
            throw new NotFoundException();
        }
        court.setId(null);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        Court finalCourt = null;
        try {
            finalCourt = mapper.readerForUpdating(optExistingCourt.get()).readValue(mapper.writeValueAsString(court));
            courtDao.save(finalCourt);
            return finalCourt;
        } catch (IOException e) {
            e.printStackTrace();
            throw new BadRequestException();
        }
    }*/
}
