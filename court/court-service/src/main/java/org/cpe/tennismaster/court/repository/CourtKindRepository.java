package org.cpe.tennismaster.court.repository;

import org.cpe.tennismaster.court.model.TypeCourt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CourtKindRepository extends JpaRepository<TypeCourt, Integer> {

    List<TypeCourt> findAllBy();

    Optional<TypeCourt> findAllByLibelleTypeCourt(String libelleTypeCourt);

}