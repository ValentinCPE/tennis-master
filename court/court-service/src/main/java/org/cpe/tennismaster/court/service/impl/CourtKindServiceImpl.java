package org.cpe.tennismaster.court.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.common.utils.UtilsTools;
import org.cpe.tennismaster.court.common.dto.TypeCourtDto;
import org.cpe.tennismaster.court.common.vue.TypeCourtVue;
import org.cpe.tennismaster.court.model.TypeCourt;
import org.cpe.tennismaster.court.repository.CourtKindRepository;
import org.cpe.tennismaster.court.service.CourtKindService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class CourtKindServiceImpl implements CourtKindService {

    private final CourtKindRepository courtKindRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    public CourtKindServiceImpl(CourtKindRepository courtKindRepository) {
        this.courtKindRepository = courtKindRepository;
    }

    public List<TypeCourtVue> getAllCourtsKind() {
        List<TypeCourt> typeCourtList =  this.courtKindRepository.findAllBy();
        Type listType = new TypeToken<List<TypeCourtVue>>() {}.getType();
        return modelMapper.map(typeCourtList, listType);
    }

    public TypeCourtVue getCourtKindById(Integer id) throws ParameterException {
        Optional<TypeCourt> optCourtKind = courtKindRepository.findById(id);
        if (!optCourtKind.isPresent()) {
            throw new ParameterException();
        }

        return modelMapper.map(optCourtKind.get(), TypeCourtVue.class);
    }

    public TypeCourtVue getCourtKindByLabel(String courtKind) throws ParameterException {
        Optional<TypeCourt> optCourtKind = courtKindRepository.findAllByLibelleTypeCourt(courtKind);
        if (!optCourtKind.isPresent()) {
            throw new ParameterException();
        }

        return modelMapper.map(optCourtKind.get(), TypeCourtVue.class);
    }

    public TypeCourtVue createCourtKind(TypeCourtDto typeCourtDto) throws NotFoundException {
        if (typeCourtDto == null) {
            throw new NotFoundException("Type court");
        }
        TypeCourt typeCourtModel = modelMapper.map(typeCourtDto, TypeCourt.class);
        typeCourtModel = courtKindRepository.save(typeCourtModel);

       return modelMapper.map(typeCourtModel, TypeCourtVue.class);
    }

    public Void deleteCourtKind(Integer id) throws NotFoundException {
        Optional<TypeCourt> optCourtKind = courtKindRepository.findById(id);
        if (!optCourtKind.isPresent()) {
            throw new NotFoundException("Type court");
        }
        courtKindRepository.delete(optCourtKind.get());
        return null;
    }

    public TypeCourtVue updateCourtKind(Integer id, TypeCourtDto typeCourtDto) throws NotFoundException {
        if (typeCourtDto == null) {
            throw new NotFoundException("Type court");
        }
        Optional<TypeCourt> optExistingCourtKind = courtKindRepository.findById(id);
        if (!optExistingCourtKind.isPresent()) {
            throw new NotFoundException("Type court");
        }
        TypeCourt typeCourtModel = modelMapper.map(typeCourtDto, TypeCourt.class);
        UtilsTools.copyNonNullProperties(typeCourtModel, optExistingCourtKind.get());
        typeCourtModel = courtKindRepository.save(optExistingCourtKind.get());

        if (typeCourtModel == null) {
            throw new NotFoundException("Type court");
        }
        return modelMapper.map(typeCourtModel, TypeCourtVue.class);
    }
}
