package org.cpe.tennismaster.court.service;

import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.court.common.dto.TypeCourtDto;
import org.cpe.tennismaster.court.common.vue.TypeCourtVue;
import org.cpe.tennismaster.court.model.TypeCourt;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CourtKindService {

    List<TypeCourtVue> getAllCourtsKind();

    TypeCourtVue getCourtKindById(Integer id) throws ParameterException;

    TypeCourtVue getCourtKindByLabel(String courtKind) throws ParameterException;

    TypeCourtVue createCourtKind(TypeCourtDto typeCourtDto) throws NotFoundException;

    Void deleteCourtKind(Integer id) throws NotFoundException;

    TypeCourtVue updateCourtKind(Integer id, TypeCourtDto typeCourtDto) throws NotFoundException;
}
