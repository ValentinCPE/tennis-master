package org.cpe.tennismaster.court.common.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotNull;
import java.util.Set;

public class TypeCourtDto {

    private Integer idTypeCourt;

    @NotNull
    private String libelleTypeCourt;

    private Set<CourtDto> courts;

    public TypeCourtDto() {
    }

    public TypeCourtDto(String libelleTypeCourt) {
        this.libelleTypeCourt = libelleTypeCourt;
    }

    public Integer getIdTypeCourt() {
        return idTypeCourt;
    }

    public void setIdTypeCourt(Integer idTypeCourt) {
        this.idTypeCourt = idTypeCourt;
    }

    public String getLibelleTypeCourt() {
        return libelleTypeCourt;
    }

    public void setLibelleTypeCourt(String labelType) {
        this.libelleTypeCourt = labelType;
    }

    @JsonIgnore
    public Set<CourtDto> getCourts() {
        return courts;
    }

    public void setCourts(Set<CourtDto> courts) {
        this.courts = courts;
    }
}