package model;

import lombok.*;

import java.util.UUID;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserAuthorizationDto {

    private String email;

    private String password;

}
