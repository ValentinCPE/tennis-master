package exception;

import org.cpe.tennismaster.common.exception.GeneralException;
import org.springframework.http.HttpStatus;

public class ActivationException extends GeneralException {

    public ActivationException(){
        super();
    }

    @Override
    protected HttpStatus defineStatusHttp() {
        return HttpStatus.CONFLICT;
    }

    @Override
    protected String defineErrorMessage(String variable) {
        return "Utilisateur n'est pas activé.";
    }

}
