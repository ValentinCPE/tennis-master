package org.cpe.tennismaster.auth.controller;

import org.cpe.tennismaster.auth.configuration.AuthorizationServerConfiguration;
import org.cpe.tennismaster.auth.service.AuthorizationService;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Resource(name="redisTokenStore")
    TokenStore tokenStore;

    @Resource(name="tokenServices")
    ConsumerTokenServices tokenServices;

    private final Environment environment;

    private final AuthorizationService authorizationService;

    public UserController(Environment environment, AuthorizationService authorizationService) {
        this.environment = environment;
        this.authorizationService = authorizationService;
    }

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/deconnecterTout")
    public ResponseEntity<String> deconnecterTout(@RequestHeader(name="Authorization") String token){
        token = token.split(" ")[1];
        List<String> tokens = this.getAllTokens(token);
        this.revokeTokens(tokens);
        return ResponseEntity.ok("OK");
    }

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/tokens")
    public List<String> voir(){
        return this.getAllTokens(null);
    }

    @RequestMapping("/validateUser")
    public Principal user(Principal user) {
        return user;
    }

    @GetMapping("/generatePassword")
    public String generatePassword(@RequestParam(value = "password") String password){
        return this.authorizationService.generatePassword(password);
    }

    private List<String> getAllTokens(String tokenAGarder){
        List<String> tokenValues = new ArrayList<>();
        Collection<OAuth2AccessToken> tokensAdminWebs = tokenStore.findTokensByClientId(AuthorizationServerConfiguration.clientIdAdminWeb);
        Collection<OAuth2AccessToken> tokensAmazon = tokenStore.findTokensByClientId(AuthorizationServerConfiguration.clientIdAmazon);
        if (tokensAdminWebs!=null){
            for (OAuth2AccessToken token:tokensAdminWebs){
                if(!tokenValues.contains(token.getValue()) && !token.equals(tokenAGarder)){
                    tokenValues.add(token.getValue());
                }
            }
        }
        if (tokensAmazon!=null){
            for (OAuth2AccessToken token:tokensAmazon){
                if(!tokenValues.contains(token.getValue()) && !token.equals(tokenAGarder)){
                    tokenValues.add(token.getValue());
                }
            }
        }
        return tokenValues;
    }

    private void revokeTokens(List<String> tokens){
        for(String token : tokens){
            tokenServices.revokeToken(token);
        }
    }

}
