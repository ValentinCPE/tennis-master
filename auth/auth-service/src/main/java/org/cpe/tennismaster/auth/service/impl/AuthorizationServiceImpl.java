package org.cpe.tennismaster.auth.service.impl;

import org.cpe.tennismaster.auth.model.UtilisateurVue;
import org.cpe.tennismaster.auth.repository.UtilisateurVueRepository;
import org.cpe.tennismaster.auth.service.AuthorizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthorizationServiceImpl implements AuthorizationService {

    private static final Logger logger = LoggerFactory.getLogger(AuthorizationServiceImpl.class);

    private final PasswordEncoder passwordEncoder;

    private final UtilisateurVueRepository utilisateurVueRepository;

    public AuthorizationServiceImpl(PasswordEncoder passwordEncoder, UtilisateurVueRepository utilisateurVueRepository) {
        this.passwordEncoder = passwordEncoder;
        this.utilisateurVueRepository = utilisateurVueRepository;
    }

    @Override
    public UtilisateurVue getUser(String login) {
        if(login == null || login.isEmpty()) return null;
        Optional<UtilisateurVue> utilisateurVue = utilisateurVueRepository.findUtilisateurVueByLogin(login);
        return utilisateurVue.orElse(null);
    }

    @Override
    public String generatePassword(String password) {
        return passwordEncoder.encode(password);
    }

}
