package org.cpe.tennismaster.auth.repository;

import org.cpe.tennismaster.auth.model.UtilisateurVue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UtilisateurVueRepository extends JpaRepository<UtilisateurVue, Integer> {

    Optional<UtilisateurVue> findUtilisateurVueByLogin(String login);

}