package handlers;

import com.amazon.ask.attributes.AttributesManager;
import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.*;
import com.amazon.ask.model.slu.entityresolution.Resolution;
import com.amazon.ask.model.slu.entityresolution.StatusCode;
import com.amazon.ask.model.slu.entityresolution.ValueWrapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import model.ReservationParCourtVue;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.amazon.ask.request.Predicates.intentName;
import static org.slf4j.LoggerFactory.getLogger;

public class GetAvailaibilitiesCourtsRequestHandler implements RequestHandler {

    private static Logger logger = getLogger(GetAvailaibilitiesCourtsRequestHandler.class);

    @Override
    public boolean canHandle(HandlerInput handlerInput) {
        return handlerInput.matches(intentName("GetAvailabilitiesCourts"));
    }

    @Override
    public Optional<Response> handle(HandlerInput handlerInput) {
        String token = handlerInput.getRequestEnvelope()
                .getContext()
                .getSystem()
                .getUser()
                .getAccessToken();
        logger.info("token : " + token);

        if(token == null || token.isEmpty()){
            return AlexaUtil.getNoTokenSpeech(handlerInput);
        }

        try {
            Request request = handlerInput.getRequestEnvelope().getRequest();
            IntentRequest intentRequest = (IntentRequest) request;
            Intent intent = intentRequest.getIntent();
            Map<String, Slot> slots = intent.getSlots();

            logger.info("Slots : {}", slots.toString());

            if(slots.get(TennisMasterHandler.libelleCourt) != null &&
                slots.get(TennisMasterHandler.libelleCourt).getResolutions() != null &&
                slots.get(TennisMasterHandler.libelleCourt).getResolutions().getResolutionsPerAuthority() != null &&
                slots.get(TennisMasterHandler.libelleCourt).getResolutions().getResolutionsPerAuthority().size() > 0){

                String libelleCourtString = "";
                List<Resolution> resolutions = slots.get(TennisMasterHandler.libelleCourt).getResolutions().getResolutionsPerAuthority();
                for(Resolution resolution : resolutions){
                    if(resolution.getStatus().getCode() == StatusCode.ER_SUCCESS_MATCH){
                        for(ValueWrapper valueWrapper : resolution.getValues()){
                            libelleCourtString = valueWrapper.getValue().getName();
                            break;
                        }
                        break;
                    }
                }

                String speechText;

                if(!libelleCourtString.isEmpty()){
                    AttributesManager attributesManager = handlerInput.getAttributesManager();
                    Map<String,Object> attributes = attributesManager.getSessionAttributes();

                    AlexaUtil.speakWhileWaiting(handlerInput, "Je suis en train de verifier !");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

                    logger.info("Getting Court Availabilities : {}", libelleCourtString);

                    Map<String, Object> map = new HashMap<>();
                    map.put("dateReservation", dateFormat.format(new Date()));
                    map.put("libelleCourt", libelleCourtString);
                    String response = WebRequest.doRequest(TennisMasterHandler.apiUrlReservation + "reservations/dispo", "GET", map, token);
                    logger.info(response);

                    Gson g = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create();
                    Type collectionType = new TypeToken<Collection<ReservationParCourtVue>>(){}.getType();
                    Collection<ReservationParCourtVue> disponibilites = g.fromJson(response, collectionType);

                    Integer idCourt = null;
                    Date firstDateToBeBooked = null;

                    if(disponibilites.size() > 0) {
                        ReservationParCourtVue reservationParCourtVue = disponibilites.iterator().next();
                        idCourt = reservationParCourtVue.getIdCourt();
                        List<Date> datesDisponibles = reservationParCourtVue.getDatesByReservationParCourtVue();
                        firstDateToBeBooked = AlexaUtil.firstDateAfterNowWithListOfDate(datesDisponibles);
                    }

                    if(firstDateToBeBooked == null){
                        return handlerInput.getResponseBuilder()
                                .withSimpleCard("TennisMaster", "Il est trop tard pour reserver pour aujourd'hui ! Reessayez demain .")
                                .withSpeech("Il est trop tard pour reserver pour aujourd'hui ! Reessayez demain .")
                                .withShouldEndSession(true)
                                .build();
                    }

                    SimpleDateFormat firstDateToBeBookedFormatSaid = new SimpleDateFormat("HH:mm");
                    String heureSaidString = firstDateToBeBookedFormatSaid.format(firstDateToBeBooked);

                    logger.info("Hour said : {}", heureSaidString);

                    SimpleDateFormat firstDateToBeBookedFormatRequest = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    String heureRequestString = firstDateToBeBookedFormatRequest.format(firstDateToBeBooked);

                    attributes.put(TennisMasterHandler.idCourt, idCourt);
                    attributes.put(TennisMasterHandler.libelleCourt, libelleCourtString);
                    attributes.put(TennisMasterHandler.heure, heureRequestString);

                    attributesManager.setSessionAttributes(attributes);

                    speechText =
                            String.format("La prochaine disponibilitee pour le court %s est : <break time=\"500ms\"/> "
                                    + "<say-as interpret-as=\"time\">%s</say-as>. <break time=\"1s\"/> Pour effectuer la reservation, dites : reserve-moi ce court ! <break time=\"500ms\"/> Si vous ne dites rien, alors a la prochaine fois ! ", libelleCourtString, heureSaidString);
                } else {
                    throw new Exception("Probleme avec numCourt ou heure");
                }

                return handlerInput.getResponseBuilder()
                        .withSimpleCard("TennisMaster", "Nous venons d'enoncer la disponibilité. Vous pouvez la redemander .")
                        .withSpeech(speechText)
                        .withShouldEndSession(false)
                        .build();
            } else {
                logger.error("Error resolutions : {}", slots.get(TennisMasterHandler.libelleCourt).getResolutions());
                return AlexaUtil.getErrorSpeech(handlerInput);
            }

        } catch (Exception e) {
            logger.error("Error while executing script : {}", e.getMessage());
            return AlexaUtil.getErrorSpeech(handlerInput);
        }
    }

}
