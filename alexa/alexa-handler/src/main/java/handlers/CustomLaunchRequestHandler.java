package handlers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.impl.LaunchRequestHandler;
import com.amazon.ask.model.LaunchRequest;
import com.amazon.ask.model.Response;
import com.amazon.ask.model.dialog.DynamicEntitiesDirective;
import com.amazon.ask.model.er.dynamic.Entity;
import com.amazon.ask.model.er.dynamic.EntityListItem;
import com.amazon.ask.model.er.dynamic.EntityValueAndSynonyms;
import com.amazon.ask.model.er.dynamic.UpdateBehavior;
import com.amazon.ask.model.ui.Image;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import model.CourtLibelleVue;
import model.ReservationParCourtVue;
import org.slf4j.Logger;

import java.lang.reflect.Type;
import java.util.*;
import java.util.function.Predicate;

import static com.amazon.ask.request.Predicates.requestType;
import static org.slf4j.LoggerFactory.getLogger;

public class CustomLaunchRequestHandler implements LaunchRequestHandler {
    private static Logger logger = getLogger(CustomLaunchRequestHandler.class);

    @Override
    public boolean canHandle(HandlerInput input, LaunchRequest launchRequest) {
        return input.matches(requestType(LaunchRequest.class));
    }

    @Override
    public Optional<Response> handle(HandlerInput input, LaunchRequest launchRequest) {
        logger.debug("Lancement skill: " + input.getRequestEnvelopeJson());

        AlexaUtil.speakWhileWaiting(input, "<p>Bienvenue dans votre club de tennis !</p><p>Avant de commencer, reecuperons les courts de tennis disponibles !</p>");

        try {

            String response = WebRequest.doRequest(TennisMasterHandler.apiUrlCourt + "court/libelles", "POST", new HashMap<>(), null);

            Gson g = new Gson();
            Type collectionType = new TypeToken<Collection<CourtLibelleVue>>(){}.getType();
            Collection<CourtLibelleVue> courtLibelleVues = g.fromJson(response, collectionType);

            if(courtLibelleVues.size() == 0) {
                logger.info("No court fetched ! ");
                return input.getResponseBuilder()
                        .withSpeech("Il n'existe pas de court ! Demandez a l'administrateur de votre club d'ajouter un court dans Tennis Master !")
                        .withShouldEndSession(true)
                        .build();
            }

            List<Entity> entities = new ArrayList<>();

            for(CourtLibelleVue courtLibelleVue : courtLibelleVues) {
                String libelleCourt = courtLibelleVue.getLibelleCourt();

                String[] synonymsLibelleCourt = libelleCourt.split(" ");
                List<String> synonymsCourts = new ArrayList<>();

                for(String synonym : synonymsLibelleCourt){
                    if(!synonym.toLowerCase().contains("tennis") && synonym.length() > 2){
                        synonymsCourts.add(synonym);
                    }
                }

                Entity entity = Entity.builder()
                        .withId("")
                        .withName(EntityValueAndSynonyms.builder()
                                .withValue(libelleCourt)
                                .withSynonyms(synonymsCourts)
                                .build())
                        .build();

                entities.add(entity);
            }

            DynamicEntitiesDirective replaceEntityDirective = DynamicEntitiesDirective
                    .builder()
                    .addTypesItem(EntityListItem.builder()
                            .withName(TennisMasterHandler.libelleCourt)
                            .withValues(entities)
                            .build())
                    .withUpdateBehavior(UpdateBehavior.REPLACE)
                    .build();

            String speechText =
                    String.format("<p>Dans cette application, vous pouvez savoir les disponibilitees de vos courts de tennis favoris pour aujourd'hui et meime les reeserver . </p>" +
                            "<p>Exemple de phrase pour commencer : <break time=\"1s\"/> Quelle est la prochaine disponibilitee du court %s ? <break time=\"1s\"/> Vous pourrez dans un second temps le reeserver . </p>", entities.get(0).getName().getValue());

            return input.getResponseBuilder()
                    .withSpeech(speechText)
                    .withStandardCard("Tennis Master", "Bienvenue sur l'application Tennis Master", Image.builder().withLargeImageUrl("https://image.freepik.com/vecteurs-libre/logo-du-championnat-tennis-american-logo-sport_1366-137.jpg").build())
                    .addDirective(replaceEntityDirective)
                    .withShouldEndSession(false)
                    .build();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return AlexaUtil.getErrorSpeech(input);
        }
    }
}
