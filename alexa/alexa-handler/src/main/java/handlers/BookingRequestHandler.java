package handlers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.amazon.ask.request.Predicates.intentName;
import static org.slf4j.LoggerFactory.getLogger;

public class BookingRequestHandler implements RequestHandler {

    private static Logger logger = getLogger(BookingRequestHandler.class);

    @Override
    public boolean canHandle(HandlerInput handlerInput) {
        return handlerInput.matches(intentName("Booking"));
    }

    @Override
    public Optional<Response> handle(HandlerInput handlerInput) {
        String token = handlerInput.getRequestEnvelope()
                .getContext()
                .getSystem()
                .getUser()
                .getAccessToken();
        logger.info("token : " + token);

        if(token == null || token.isEmpty()){
            return AlexaUtil.getNoTokenSpeech(handlerInput);
        }

        try {
            Map<String, Object> sessionAttributes = handlerInput.getAttributesManager().getSessionAttributes();
            if (!sessionAttributes.containsKey(TennisMasterHandler.libelleCourt) || !sessionAttributes.containsKey(TennisMasterHandler.heure)){
                return handlerInput.getResponseBuilder()
                        .withSpeech("Il est pour l'instant impossible de reserver car vous n'avez pas demandee la disponibilite d'un court en premier lieu ! <break time=\"1s\"/>" +
                                "Recommencez l'interaction !")
                        .withShouldEndSession(true)
                        .build();
            }

            String idCourt = sessionAttributes.get(TennisMasterHandler.idCourt).toString();
            String libelleCourt = sessionAttributes.get(TennisMasterHandler.libelleCourt).toString();
            String heure = sessionAttributes.get(TennisMasterHandler.heure).toString();
            handlerInput.getAttributesManager().setSessionAttributes(new HashMap<>());

            AlexaUtil.speakWhileWaiting(handlerInput, "Je suis en train de reserver le terrain pour vous ! !");

            logger.info("Id court : {} | Libelle court : {} | Heure : {}", idCourt, libelleCourt, heure);

            Map<String, Object> map = new HashMap<>();
            map.put("idCourt", Integer.valueOf(idCourt));
            map.put("dateDebutReservation", heure);
            map.put("valide", false);
            WebRequest.doRequest(TennisMasterHandler.apiUrlReservation + "reservations/user", "POST", map, token);

            SimpleDateFormat dateBooked = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date heureDate = dateBooked.parse(heure);

            SimpleDateFormat dateBookedFormatString = new SimpleDateFormat("HH:mm");
            String heureString = dateBookedFormatString.format(heureDate);

            String cardText = String.format("Vous venez de réserver le court %s à %s. Retrouvez votre QRCode dans l'application Tennis Master !", libelleCourt, heureString);
            String speechText = String.format("Vous venez de reserver le court %s a : <break time=\"500ms\"/> "
                    + "<say-as interpret-as=\"time\">%s</say-as>. Vous retrouverez votre QR code dans l'application Tennis Master . <break time=\"1s\"/> Merci beaucoup d'avoir utiliser notre assistant vocal ! <break time=\"500ms\"/> <say-as interpret-as=\"interjection\">Au revoir</say-as>", libelleCourt, heureString);

            return handlerInput.getResponseBuilder()
                    .withSimpleCard("TennisMaster", cardText)
                    .withSpeech(speechText)
                    .withShouldEndSession(true)
                    .build();
        } catch (Exception e) {
            logger.error("Error while executing script : {}", e.getMessage());
            return AlexaUtil.getErrorSpeech(handlerInput);
        }
    }

}
