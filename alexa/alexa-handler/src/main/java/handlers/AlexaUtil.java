package handlers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.RequestEnvelope;
import com.amazon.ask.model.Response;
import com.amazon.ask.model.services.directive.*;
import org.slf4j.Logger;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.slf4j.LoggerFactory.getLogger;

public class AlexaUtil {

    private static Logger logger = getLogger(AlexaUtil.class);

    public static void speakWhileWaiting(HandlerInput handlerInput, String textToSay){
        RequestEnvelope requestEnvelope = handlerInput.getRequestEnvelope();
        DirectiveServiceClient directiveServiceClient = handlerInput.getServiceClientFactory().getDirectiveService();

        String requestId = requestEnvelope.getRequest().getRequestId();

        Header header = Header.builder()
                .withRequestId(requestId)
                .build();

        Directive directive = SpeakDirective.builder()
                .withSpeech(textToSay)
                .build();

        SendDirectiveRequest directiveRequest = SendDirectiveRequest.builder()
                .withHeader(header)
                .withDirective(directive)
                .build();

        directiveServiceClient.enqueue(directiveRequest);
    }

    public static Optional<Response> getErrorSpeech(HandlerInput handlerInput) {
        return handlerInput.getResponseBuilder()
                .withSpeech("Les serveurs de Tennis Master sont actuellement hors service ! Reessayez plus tard !")
                .withShouldEndSession(true)
                .build();
    }

    public static Optional<Response> getNoTokenSpeech(HandlerInput handlerInput) {
        return handlerInput.getResponseBuilder()
                .withSpeech("Vous ne vous eites jamais connectees a l'application TennisMaster. Cette etape est necessaire pour reserver un court ! Voir dans l'application Alexa pour plus de detail !")
                .withLinkAccountCard()
                .withShouldEndSession(true)
                .build();
    }

    public static Date firstDateAfterNowWithListOfDate(List<Date> dates){
        Date now = new Date();
        now = addHoursToJavaUtilDate(now, 2); // Add one hour to the list of possible courts to be bookable
        logger.info("Now + 1 hour = {}", now.toString());
        Date firstDate = null;
        for (Date date : dates) {
            if (now.before(date)) {
                firstDate = date;
                break;
            }
        }
        if(firstDate != null){
            logger.info("Date found : {}", firstDate.toString());
        }
        return firstDate;
    }

    private static Date addHoursToJavaUtilDate(Date date, int hours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, hours);
        return calendar.getTime();
    }

}
