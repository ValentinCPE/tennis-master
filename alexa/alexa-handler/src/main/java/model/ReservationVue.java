package model;

import java.util.Date;

public class ReservationVue {

    private Integer idReservation;

    private Date dateDebutReservation;

    private Date dateCreationReservation;

    private Boolean valide;

    public ReservationVue(Integer idReservation, Date dateDebutReservation, Date dateCreationReservation, Boolean valide) {
        this.idReservation = idReservation;
        this.dateDebutReservation = dateDebutReservation;
        this.dateCreationReservation = dateCreationReservation;
        this.valide = valide;
    }

    public Integer getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(Integer idReservation) {
        this.idReservation = idReservation;
    }

    public Date getDateDebutReservation() {
        return dateDebutReservation;
    }

    public void setDateDebutReservation(Date dateDebutReservation) {
        this.dateDebutReservation = dateDebutReservation;
    }

    public Date getDateCreationReservation() {
        return dateCreationReservation;
    }

    public void setDateCreationReservation(Date dateCreationReservation) {
        this.dateCreationReservation = dateCreationReservation;
    }

    public Boolean getValide() {
        return valide;
    }

    public void setValide(Boolean valide) {
        this.valide = valide;
    }

}
