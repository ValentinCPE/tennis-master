package model;

import java.util.*;

public class ReservationParCourtVue {

    private Integer idCourt;

    private List<ReservationVue> reservationVueList;

    public ReservationParCourtVue(Integer idCourt, List<ReservationVue> reservationVueList) {
        this.idCourt = idCourt;
        this.reservationVueList = reservationVueList;
    }

    public Integer getIdCourt() {
        return idCourt;
    }

    public void setIdCourt(Integer idCourt) {
        this.idCourt = idCourt;
    }

    public List<ReservationVue> getReservationVueList() {
        return reservationVueList;
    }

    public void setReservationVueList(List<ReservationVue> reservationVueList) {
        this.reservationVueList = reservationVueList;
    }

    public List<Date> getDatesByReservationParCourtVue() {
        List<Date> dates = new ArrayList<>();

        this.reservationVueList.forEach(reservationVue -> {
            if(reservationVue.getDateCreationReservation() == null){
                dates.add(reservationVue.getDateDebutReservation());
            }
        });

        return dates;
    }

}
