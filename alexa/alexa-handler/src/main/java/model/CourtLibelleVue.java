package model;

public class CourtLibelleVue {

    private String libelleCourt;

    public CourtLibelleVue(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    public String getLibelleCourt() {
        return libelleCourt;
    }

    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }
}
