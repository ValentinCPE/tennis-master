package org.cpe.tennismaster.notification.service;

import org.cpe.tennismaster.notification.common.model.NotificationDto;
import org.springframework.stereotype.Service;

@Service
public interface NotificationService {

    void envoi(NotificationDto notificationDto);

}
