package org.cpe.tennismaster.notification.model;

import com.kinoroy.expo.push.Priority;
import lombok.*;
import org.cpe.tennismaster.notification.common.model.IntentNotification;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.Instant;

@Document(collection = "etat_envoi")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EtatEnvoi {

    @Id
    @Indexed
    private String id;

    private Instant date;

    private IntentNotification action;

    @Indexed
    private String adresseEmetteur;

    @Indexed
    private String adresseDestinataireMail;

    private String sujetMail;

    @Indexed
    private String tokenPhone;

    private String titlePhone;

    private String bodyPhone;

    private Priority priorityPhone;

    private Integer badgePhone;

    private boolean etatEnvoi = true;

}
