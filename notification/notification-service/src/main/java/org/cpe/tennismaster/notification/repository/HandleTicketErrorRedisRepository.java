package org.cpe.tennismaster.notification.repository;

import org.cpe.tennismaster.notification.model.HandleTicketErrorRedis;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HandleTicketErrorRedisRepository extends CrudRepository<HandleTicketErrorRedis, Long> {

    Optional<HandleTicketErrorRedis> findByIdAVerifier(String id);

}
