package org.cpe.tennismaster.notification.service.impl;

import com.google.common.collect.Lists;
import com.kinoroy.expo.push.*;
import org.cpe.tennismaster.notification.common.exception.PushNotificationsNotSentException;
import org.cpe.tennismaster.notification.common.model.IntentNotification;
import org.cpe.tennismaster.notification.common.model.PushDto;
import org.cpe.tennismaster.notification.common.model.PushUtilisateurDto;
import org.cpe.tennismaster.notification.model.EtatEnvoi;
import org.cpe.tennismaster.notification.model.HandleTicketErrorRedis;
import org.cpe.tennismaster.notification.repository.EtatEnvoiRepository;
import org.cpe.tennismaster.notification.repository.HandleTicketErrorRedisRepository;
import org.cpe.tennismaster.notification.service.PushService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PushServiceImpl implements PushService {

    private static final Logger logger = LoggerFactory.getLogger(PushServiceImpl.class);

    private final HandleTicketErrorRedisRepository handleTicketErrorRedisRepository;

    private final EtatEnvoiRepository etatEnvoiRepository;

    @Autowired
    public PushServiceImpl(HandleTicketErrorRedisRepository handleTicketErrorRedisRepository, EtatEnvoiRepository etatEnvoiRepository) {
        this.handleTicketErrorRedisRepository = handleTicketErrorRedisRepository;
        this.etatEnvoiRepository = etatEnvoiRepository;
    }

    // Lien librairie : https://github.com/kinoroy/Expo-Server-SDK-Java

    @Override
    public String sendOnePushNotification(PushDto notifications) throws PushNotificationsNotSentException {
        PushUtilisateurDto pushUtilisateurDto = notifications.getNotifications().get(0);
        if(pushUtilisateurDto == null){
            throw new PushNotificationsNotSentException("Pas de notification à envoyer");
        }
        this.sendMultiplePushNotification(notifications);
        this.generateAndSaveOneEtatEnvoi(pushUtilisateurDto, notifications.getAction(), true);
        return "OK";
    }

    @Override
    public String sendMultiplePushNotification(PushDto notifications) throws PushNotificationsNotSentException {
        StringBuilder errorsNotificationsSent = new StringBuilder();

        List<PushUtilisateurDto> correctTokens = new ArrayList<>();
        for (PushUtilisateurDto notification : notifications.getNotifications()) {
            if (!ExpoPushClient.isExpoPushToken(notification.getTokenPhone())) {
                errorsNotificationsSent.append("- ").append(notification.getEmail()).append(" | ").append(notification.getTokenPhone()).append(" (Raison : Token invalide - Pas un token Expo)\n");
                // logger.error(notification.getTokenPhone() + " is not a valid Expo Push Token!");
            }else{
                correctTokens.add(notification);
            }
        }

        List<List<Message>> messages = this.getChunksMessage(correctTokens);
        messages.forEach(list100Messages -> {
            try {
                PushTicketResponse response = ExpoPushClient.sendPushNotifications(list100Messages);
                errorsNotificationsSent.append(this.manageErrors(response));
            } catch (IOException e) {
                errorsNotificationsSent.append("- ").append(e.getMessage()).append(" (Raison : Erreur interne au serveur)\n");
            }
        });

        if(errorsNotificationsSent.length() > 0){
            throw new PushNotificationsNotSentException(errorsNotificationsSent.toString());
        }

        return "OK";
    }

    private void generateAndSaveOneEtatEnvoi(PushUtilisateurDto pushUtilisateurDto, IntentNotification intentNotification, boolean etat){
        EtatEnvoi etatEnvoi = EtatEnvoi.builder()
                .date(Instant.now())
                .adresseDestinataireMail(pushUtilisateurDto.getEmail())
                .action(intentNotification)
                .tokenPhone(pushUtilisateurDto.getTokenPhone())
                .titlePhone(pushUtilisateurDto.getTitle())
                .bodyPhone(pushUtilisateurDto.getBody())
                .priorityPhone(pushUtilisateurDto.getPriority())
                .badgePhone(pushUtilisateurDto.getBadge())
                .etatEnvoi(etat)
                .build();
        etatEnvoiRepository.save(etatEnvoi);
    }

    private List<List<Message>> getChunksMessage(List<PushUtilisateurDto> correctTokens){
        List<Message> messages = new ArrayList<>();
        correctTokens.forEach(correctToken -> {
            Message message = new Message.Builder()
                    .to(correctToken.getTokenPhone())
                    .title(correctToken.getTitle())
                    .body(correctToken.getBody())
                    .priority(correctToken.getPriority())
                    .badge(correctToken.getBadge())
                    .data(correctToken.getData())
                    .build();
            messages.add(message);
        });
        return ExpoPushClient.chunkItems(messages);
    }

    private String manageErrors(PushTicketResponse response){
        StringBuilder errorsNotificationsSent = new StringBuilder();
        List<ExpoError> errors = response.getErrors();

        if (errors != null) {
            errors.forEach(error -> {
                errorsNotificationsSent.append("- ").append(error.getCode()).append(" | ").append(error.getMessage()).append(" (Raison : Erreur globale lors de l'envoi des messages)\n");
            });
        }

        List<PushTicket> tickets = response.getTickets();
        if (tickets != null) {
            for (PushTicket ticket : tickets) {
                if (ticket.getStatus() == Status.OK) {
                    String id = ticket.getId();
                    HandleTicketErrorRedis handleTicketErrorRedis = HandleTicketErrorRedis.builder()
                            .idAVerifier(id)
                            .dateAjout(new Date())
                            .build();
                    handleTicketErrorRedisRepository.save(handleTicketErrorRedis);
                } else {
                    PushError e = ticket.getDetails().getError();
                    errorsNotificationsSent.append("- ").append(ticket.getStatus()).append(" | ").append(ticket.getMessage());
                    errorsNotificationsSent.append(this.getMessageFromPushError(e));
                }
            }
        }

        return errorsNotificationsSent.toString();
    }

   // @Scheduled(cron = "0 0/15 * * * *")
    public void analyseErreursMessages(){
        logger.info("------ En train de vérifier si des erreurs sont présentes dans toutes les notifications envoyées ------");

        Iterable<HandleTicketErrorRedis> idsIterable = handleTicketErrorRedisRepository.findAll();
        List<HandleTicketErrorRedis> ids = Lists.newArrayList(idsIterable);

        if(ids.size() > 0){
            try {
                StringBuilder debugNotificationsSent = new StringBuilder();
                List<String> idsString = ids.stream()
                        .map(HandleTicketErrorRedis::getIdAVerifier)
                        .collect(Collectors.toList());
                PushReceiptResponse response = ExpoPushClient.getPushReciepts(idsString);
                Map<String, PushReceipt> receipts = response.getReceipts();

                ids.forEach(id -> {
                    PushReceipt rec = receipts.get(id.getIdAVerifier());
                    if (rec != null) {
                        debugNotificationsSent.append("- ").append(rec.getStatus()).append(" | ").append(rec.getMessage());
                        if (rec.getStatus() == Status.OK) {
                            if((new Date()).getTime() - id.getDateAjout().getTime() > 1800000){ // Si plus de 30 minutes que la notification a été envoyée, supprimer de redis
                                debugNotificationsSent.append(" (Raison : Notification envoyée correctement : ").append(id.getIdAVerifier()).append("\n");
                                handleTicketErrorRedisRepository.delete(id);
                            }
                        } else {
                            PushError e = rec.getDetails().getError();
                            debugNotificationsSent.append(this.getMessageFromPushError(e));
                            handleTicketErrorRedisRepository.delete(id);
                        }
                    }
                });
                logger.debug(debugNotificationsSent.toString());
            } catch (IOException e) {
                logger.error("Erreur interne au serveur");
            }
        }

        logger.info("------ Fin de vérification des notifications ------");
    }

    private String getMessageFromPushError(PushError pushError){
        switch (pushError) {
            case MESSAGE_TOO_BIG:
                return " (Raison : Erreur dans une notification spécifique - Message trop grand)\n";
            case INVALID_CREDENTIALS:
                return " (Raison : Erreur dans une notification spécifique - Mauvais credentials)\n";
            case DEVICE_NOT_REGISTERED:
                return " (Raison : Erreur dans une notification spécifique - Telephone non enregistré)\n";
            case MESSAGE_RATE_EXCEEDED:
                return " (Raison : Erreur dans une notification spécifique - Message Rate Exceeded)\n";
        }
        return "";
    }

}
