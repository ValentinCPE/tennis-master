package org.cpe.tennismaster.notification.service;

import org.cpe.tennismaster.notification.common.exception.PushNotificationsNotSentException;
import org.cpe.tennismaster.notification.common.model.PushDto;
import org.springframework.stereotype.Service;

@Service
public interface PushService {

    String sendOnePushNotification(PushDto notifications) throws PushNotificationsNotSentException;

    String sendMultiplePushNotification(PushDto notifications) throws PushNotificationsNotSentException;

}
