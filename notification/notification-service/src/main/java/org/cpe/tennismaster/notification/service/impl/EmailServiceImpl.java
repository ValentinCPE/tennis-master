package org.cpe.tennismaster.notification.service.impl;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.cpe.tennismaster.notification.common.model.MailDto;
import org.cpe.tennismaster.notification.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.File;
import java.io.IOException;
import java.util.*;

@Service
public class EmailServiceImpl implements EmailService {

    private static final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);

    @Value(value = "${mail.address}")
    private String addresseMail;

    @Value(value = "${mail.username}")
    private String usernameMail;

    @Value(value = "${mail.password}")
    private String passwordMail;

    private final Configuration freemarkerConfig;

    public EmailServiceImpl(@Qualifier("freeMarkerConfiguration") Configuration freemarkerConfig) {
        this.freemarkerConfig = freemarkerConfig;
    }

    private void sendSimpleMessage(MailDto mailDto, String template) throws MessagingException, IOException, TemplateException {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.connectiontimeout", "15000");
        props.put("mail.smtp.timeout", "15000");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(usernameMail, passwordMail);
                    }
                }
        );

        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(mailDto.getFrom() != null ? mailDto.getFrom() : addresseMail));
        InternetAddress[] toAddresses = { new InternetAddress(mailDto.getTo()) };
        message.setRecipients(Message.RecipientType.TO, toAddresses);
        message.setSubject(mailDto.getSubject(),"UTF-8");
        message.setSentDate(new Date());

        MimeMultipart mimeMultipart = new MimeMultipart("related");

        BodyPart messageBodyPart = new MimeBodyPart();
        Template t = freemarkerConfig.getTemplate(template);
        String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, mailDto.getModel());
        messageBodyPart.setContent(html,"text/html; charset=\"UTF-8\"");

        mimeMultipart.addBodyPart(messageBodyPart);

        if (mailDto.getImagesAAjouter() != null && mailDto.getImagesAAjouter().size() > 0) {
            Set<String> setImageID = mailDto.getImagesAAjouter().keySet();
            for (String contentId : setImageID) {
                MimeBodyPart imagePart = new MimeBodyPart();
                imagePart.setHeader("Content-ID", "<" + contentId + ">");
                File fichierImage = new ClassPathResource(mailDto.getImagesAAjouter().get(contentId)).getFile();
                DataSource fds = new FileDataSource(fichierImage);
                imagePart.setDataHandler(new DataHandler(fds));
                mimeMultipart.addBodyPart(imagePart);
            }
        }

        //Ajout pièces jointes
    /*    if(mailDto.getPiecesJointes() != null && !mailDto.getPiecesJointes().isEmpty()){
            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            for(File piece : mailDto.getPiecesJointes()){
                mimeBodyPart.attachFile(piece);
            }
            mimeMultipart.addBodyPart(mimeBodyPart);
        } */

        message.setContent(mimeMultipart);

        Transport.send(message);

    }

    @Override
    public void sendMail(MailDto mailDto, String template) throws TemplateException, IOException, MessagingException {
        this.sendSimpleMessage(mailDto, template);
        logger.debug("Nouvel email envoyé à " + mailDto.getTo());
    }

}
