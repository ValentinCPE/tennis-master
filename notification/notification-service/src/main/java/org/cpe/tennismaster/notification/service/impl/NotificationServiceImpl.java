package org.cpe.tennismaster.notification.service.impl;

import freemarker.template.TemplateException;
import org.cpe.tennismaster.notification.common.exception.PushNotificationsNotSentException;
import org.cpe.tennismaster.notification.common.model.MailDto;
import org.cpe.tennismaster.notification.common.model.NotificationDto;
import org.cpe.tennismaster.notification.common.model.PushDto;
import org.cpe.tennismaster.notification.model.EtatEnvoi;
import org.cpe.tennismaster.notification.repository.EtatEnvoiRepository;
import org.cpe.tennismaster.notification.service.EmailService;
import org.cpe.tennismaster.notification.service.NotificationService;
import org.cpe.tennismaster.notification.service.PushService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@Service
public class NotificationServiceImpl implements NotificationService {

    private static final Logger logger = LoggerFactory.getLogger(NotificationServiceImpl.class);

    private final EmailService emailService;

    private final PushService pushService;

    private final EtatEnvoiRepository etatEnvoiRepository;

    @Autowired
    public NotificationServiceImpl(EmailService emailService, PushService pushService, EtatEnvoiRepository etatEnvoiRepository) {
        this.emailService = emailService;
        this.pushService = pushService;
        this.etatEnvoiRepository = etatEnvoiRepository;
    }

    @Override
    public void envoi(NotificationDto notificationDto) {
        Thread threadEnvoi = new Thread(() -> {
            EtatEnvoi etatEnvoi = null;
            try {

                if(notificationDto.getAction() == null){
                    logger.error("Pas d'action renseignée donc pas de template");
                    return;
                }

                switch(notificationDto.getNotificationType()){
                    case MAIL:
                        MailDto mailDto = (MailDto) notificationDto;
                        etatEnvoi = EtatEnvoi.builder()
                                .date(Instant.now())
                                .action(mailDto.getAction())
                                .adresseEmetteur(mailDto.getFrom())
                                .adresseDestinataireMail(mailDto.getTo())
                                .sujetMail(mailDto.getSubject())
                                .etatEnvoi(true)
                                .build();

                        emailService.sendMail(mailDto, notificationDto.getAction().getTemplateName());
                        break;

                    case PUSH:
                        PushDto pushDto = (PushDto) notificationDto;
                        try {
                            switch(pushDto.getAction()){
                                case ENVOI_UNE_NOTIFICATION:
                                    pushService.sendOnePushNotification(pushDto);
                                    break;
                                case ENVOI_MULTIPLE_NOTIFICATION:
                                    pushService.sendMultiplePushNotification(pushDto);
                                    break;
                            }
                        } catch (PushNotificationsNotSentException e) {
                            logger.error(e.getMessage());
                        }
                        break;

                    default:
                        logger.info("Impossible d'envoyer la notification car elle n'est pas supportée : " + notificationDto.toString());
                }

            } catch (TemplateException | IOException | MessagingException e) {
                if(etatEnvoi != null){
                    etatEnvoi.setEtatEnvoi(false);
                }
                logger.error("Echec lors de l'envoi d'une notification pour la raison : " + e.getMessage());
            } finally {
                if(etatEnvoi != null){
                    etatEnvoi.setAction(notificationDto.getAction());
                    saveNotificationDB(etatEnvoi);
                }
            }
        });
        threadEnvoi.start();
    }

    private void saveNotificationDB(EtatEnvoi etatEnvoi){
        this.etatEnvoiRepository.save(etatEnvoi);
        logger.info("Nouvelle notification sauvegardée en DB : " + etatEnvoi.toString());
    }

}
