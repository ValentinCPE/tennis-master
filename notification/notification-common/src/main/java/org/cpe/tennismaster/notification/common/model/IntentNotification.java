package org.cpe.tennismaster.notification.common.model;

public enum IntentNotification {

    UTILISATEUR_CREE("mailActiverCompte.ftl"), REINIT_PASSWORD("mailMotDePasseOublie.ftl"), ENVOI_UNE_NOTIFICATION("ENVOI_UNE_NOTIFICATION"), ENVOI_MULTIPLE_NOTIFICATION("ENVOI_MULTIPLE_NOTIFICATION");

    private String templateName;

    IntentNotification(String name){
        this.templateName = name;
    }

    public String getTemplateName() {
        return templateName;
    }

    @Override
    public String toString() {
        return super.toString();
    }

}
