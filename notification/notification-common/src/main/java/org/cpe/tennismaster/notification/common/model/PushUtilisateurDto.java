package org.cpe.tennismaster.notification.common.model;

import com.kinoroy.expo.push.Priority;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.HashMap;

@Data
@Builder
@AllArgsConstructor
@ToString
public class PushUtilisateurDto implements Serializable {

    private String email;

    private String tokenPhone;

    private String title;

    private String body;

    private Priority priority;

    private Integer badge;

    private HashMap<String, Object> data;

    public PushUtilisateurDto(){
        super();
    }

}
