package org.cpe.tennismaster.notification.common.model;

import lombok.*;

import java.io.Serializable;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@ToString
public class MailDto extends NotificationDto implements Serializable {

    private String to;

    private String subject;

    private String content;

    private Map<String,Object> model;

    private Map<String,String> imagesAAjouter;

    public MailDto(){
        super();
    }

    @Override
    public String toString() {
        return "Mail{" +
                "from='" + super.getFrom() + '\'' +
                ", to='" + to + '\'' +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

}
