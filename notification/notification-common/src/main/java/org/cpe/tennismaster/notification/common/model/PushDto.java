package org.cpe.tennismaster.notification.common.model;

import lombok.*;

import java.io.File;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@ToString
public class PushDto extends NotificationDto implements Serializable {

    private IntentNotification action;

    private List<PushUtilisateurDto> notifications;

    public PushDto(){
        super();
    }

}
