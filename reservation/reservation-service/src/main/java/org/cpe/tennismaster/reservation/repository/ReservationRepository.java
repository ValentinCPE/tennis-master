package org.cpe.tennismaster.reservation.repository;


import org.cpe.tennismaster.reservation.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ReservationRepository extends JpaRepository<Reservation, Integer> {

    List<Reservation> findAllBy();

    Reservation findReservationByTokenReservation(String token);

    @Query("SELECT r FROM Reservation r where r.dateDebutReservation >= :firstDayWeek AND r.dateDebutReservation < :lastDayWeek")
    List<Reservation> findByWeek(@Param("firstDayWeek") LocalDateTime firstDayWeek,
                                 @Param("lastDayWeek") LocalDateTime lastDayWeek);

    Reservation findByIdCourtAndDateDebutReservation(Integer idCourt, LocalDateTime dateDebutReservation);

    @Query("SELECT r FROM Reservation r where r.idUtilisateur1 = :idUser OR r.idUtilisateur2 = :idUser")
    Optional<List<Reservation>> findByIdUtilisateur(@Param("idUser") Integer idUser);

    @Query("SELECT r FROM Reservation r where (r.idUtilisateur1 = :idUser OR r.idUtilisateur2 = :idUser) and r.dateDebutReservation >= :date")
    List<Reservation> findByIdUserAndGreaterThanDate(@Param("idUser") Integer idUser, @Param("date") LocalDateTime date);

    @Query("SELECT r FROM Reservation r where (r.idUtilisateur1 = :idUser OR r.idUtilisateur2 = :idUser) AND r.dateDebutReservation = :dateReservation")
    Reservation findByIdUserAndDateReservation(@Param("idUser") Integer idUser, @Param("dateReservation") LocalDateTime dateReservation);

    @Query("SELECT r FROM Reservation r where r.dateDebutReservation >= :dateDebutJourneeReservation and r.dateDebutReservation < :dateDebutLendemain")
    Optional<List<Reservation>> findAllByDateDebutReservation(@Param("dateDebutJourneeReservation") LocalDateTime dateDebutJourneeReservation, @Param("dateDebutLendemain") LocalDateTime dateDebutLendemain);

    @Query("SELECT r FROM Reservation r where r.dateDebutReservation >= :dateDebutJourneeReservation and r.dateDebutReservation < :dateDebutLendemain and r.idCourt in :idCourtList")
    Optional<List<Reservation>> findAllByDateDebutReservationAndCourtIdList(@Param("dateDebutJourneeReservation") LocalDateTime dateDebutJourneeReservation, @Param("dateDebutLendemain") LocalDateTime dateDebutLendemain, @Param("idCourtList") List<Integer> idCourtList);
}
