package org.cpe.tennismaster.reservation.service;

import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.reservation.common.dto.PartieDto;
import org.cpe.tennismaster.reservation.common.vue.PartieVue;
import org.cpe.tennismaster.reservation.model.Partie;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface GameService {

    List<PartieVue> getAllGame();

    PartieVue getGameById(Integer id) throws NotFoundException;

    PartieVue createGame(PartieDto partieDto) throws ParameterException;

    Void removeGame(Integer id) throws NotFoundException;

    PartieVue updateGame(Integer id, PartieDto partieDto) throws ParameterException;
}
