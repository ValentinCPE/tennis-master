package org.cpe.tennismaster.reservation.service.impl;

import org.cpe.tennismaster.common.exception.InternalServerException;
import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.common.utils.UtilsTools;
import org.cpe.tennismaster.court.common.dto.CourtDto;
import org.cpe.tennismaster.court.common.vue.CourtVue;
import org.cpe.tennismaster.common.model.utilisateur.UtilisateurVue;
import org.cpe.tennismaster.reglesClub.common.vue.ReglesClubVue;
import org.cpe.tennismaster.reservation.common.dto.DispoReservationDto;
import org.cpe.tennismaster.reservation.common.dto.ReservationDto;
import org.cpe.tennismaster.reservation.common.exception.NoReservationPossibleException;
import org.cpe.tennismaster.reservation.common.exception.UserCourtNotExistException;
import org.cpe.tennismaster.reservation.common.vue.ReservationGrantedVue;
import org.cpe.tennismaster.reservation.common.vue.ReservationParCourtVue;
import org.cpe.tennismaster.reservation.common.vue.ReservationVue;
import org.cpe.tennismaster.reservation.model.Reservation;
import org.cpe.tennismaster.reservation.repository.ReservationRepository;
import org.cpe.tennismaster.reservation.service.ReservationService;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeToken;
import org.modelmapper.spi.MappingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static java.time.temporal.ChronoUnit.MINUTES;

@Service
public class ReservationServiceImpl implements ReservationService {

    private static final Logger logger = LoggerFactory.getLogger(ReservationServiceImpl.class);

    private final static int reservationDurationInHour = 1;

    private final ReservationRepository reservationRepository;
    
    private final UtilsTools utilsTools;

    private ModelMapper modelMapper;

    @Value(value = "http://${service-utilisateur.ip}"+":${service-utilisateur.port}"+"/${service-utilisateur.get}/")
    private String baseUrlUtilisateurGet;

    @Value(value = "http://${service-court.ip}"+":${service-court.port}"+"/${service-court.get}/")
    private String baseUrlCourtGet;

    @Value(value = "http://${service-reglesClub.ip}"+":${service-reglesClub.port}"+"/${service-reglesClub.get}/")
    private String baseUrlReglesClubGet;

    @Autowired
    public ReservationServiceImpl(ReservationRepository reservationRepository, UtilsTools utilsTools, ModelMapper modelMapper) {
        this.reservationRepository = reservationRepository;
        this.utilsTools = utilsTools;
        this.modelMapper = modelMapper;

        //Convert user and court id to userview and courtview
        Converter<Integer, UtilisateurVue> convertIdUserToUser = new Converter<Integer, UtilisateurVue>()
        {
            public UtilisateurVue convert(MappingContext<Integer, UtilisateurVue> context)
            {
                if(context.getSource() != null) {
                    return utilsTools.restCall(baseUrlUtilisateurGet + context.getSource(), HttpMethod.GET, new ParameterizedTypeReference<UtilisateurVue>() {});
                }
                return null;
            }
        };
        Converter<Integer, CourtVue> convertIdCourtToCourt = new Converter<Integer, CourtVue>()
        {
            public CourtVue convert(MappingContext<Integer, CourtVue> context)
            {
                return utilsTools.restCall(baseUrlCourtGet+context.getSource(), HttpMethod.GET, new ParameterizedTypeReference<CourtVue>() {});
            }
        };
        PropertyMap<Reservation, ReservationVue> mapReservationToReservationVue = new PropertyMap<Reservation, ReservationVue>() {
            protected void configure() {
                using(convertIdUserToUser).map(source.getIdUtilisateur1()).setIdUtilisateur1(null);
                using(convertIdUserToUser).map(source.getIdUtilisateur2()).setIdUtilisateur2(null);
                using(convertIdCourtToCourt).map(source.getIdCourt()).setIdCourt(null);
            }
        };

        modelMapper.addMappings(mapReservationToReservationVue);
    }

    public List<ReservationVue> getAllReservations() {
        List<Reservation> reservationList =  this.reservationRepository.findAllBy();
        Type listType = new TypeToken<List<ReservationVue>>() {}.getType();
        return modelMapper.map(reservationList, listType);
    }

    @Override
    public ReservationGrantedVue tokenReservationAutorise(String code) {
        boolean status = false;
        Reservation reservation = this.reservationRepository.findReservationByTokenReservation(code);
        if(reservation != null) {
            LocalDateTime now = LocalDateTime.now();
            if(reservation.getDateDebutReservation().isBefore(now)){
                status = true;
            }
        }
        logger.info("Status validation reservation : {}", status);
        return new ReservationGrantedVue(status);
    }

    public List<ReservationVue> getReservationsByWeek(String dateString) throws InternalServerException {
        LocalDateTime firstDay;
        if (dateString.isEmpty()) {
            firstDay = LocalDateTime.now();
        } else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            firstDay = LocalDateTime.parse(dateString, formatter);
        }

        LocalDateTime lastDayOfTheWeek = firstDay.plusDays(6);

        List<Reservation> reservationList = reservationRepository.findByWeek(firstDay, lastDayOfTheWeek);
        Type listType = new TypeToken<List<ReservationVue>>() {}.getType();
        return modelMapper.map(reservationList, listType);
    }

    public List<ReservationVue> getReservationsByWeek(Integer id) throws InternalServerException {
        utilsTools.restCall(baseUrlUtilisateurGet + id, HttpMethod.GET, new ParameterizedTypeReference<UtilisateurVue>() {});
        LocalDateTime today = LocalDateTime.now();
        List<Reservation> reservationList = reservationRepository.findByIdUserAndGreaterThanDate(id, today);
        Type listType = new TypeToken<List<ReservationVue>>() {}.getType();
        return modelMapper.map(reservationList, listType);
    }

    public ReservationVue getReservationById(Integer id) throws NotFoundException {
        Optional<Reservation> optReservation = reservationRepository.findById(id);
        if (!optReservation.isPresent()) {
            throw new NotFoundException("Reservation " + id);
        }
        return  modelMapper.map(optReservation.get(), ReservationVue.class);
    }

    private int getNumberOfPossibleReservation(LocalTime startLocalTime, LocalTime endLocalTime) {
        //différence entre l'heure de fin et l'heure de début
        int hourDifference = endLocalTime.getHour() - (startLocalTime.getHour());
        //si l'heure de début est plus grande ou égal à l'heure de fin, aucune réservation n'est possible
        if(hourDifference <= 0)
            return 0;
        int numberPossibleReservation = hourDifference / reservationDurationInHour;
        //S'il n'y a pas de reste au modulo et que le nombre de minutes de l'heure de début est supérieur au nombre
        // de minute de l'heure de fin, il ne restera pas 1h pour la derniere reservation donc on enlève une réservation
        if(startLocalTime.getMinute() > endLocalTime.getMinute()) {
            if(reservationDurationInHour == 1 || hourDifference % reservationDurationInHour == 0)
                numberPossibleReservation--;
        }
        return numberPossibleReservation;
    }

    private LocalDateTime changeHourOfDateFromOtherDate(LocalDate validDate, LocalTime rightHourDate) {
        LocalDateTime validLocalDateTime = validDate.atStartOfDay();

        return validLocalDateTime.with(rightHourDate);
    }

    private boolean isTheSameReservation(LocalDateTime dateReservationDispo, LocalDateTime dateReservation) {
        dateReservationDispo = dateReservationDispo.withSecond(0).withNano(0);
        dateReservation = dateReservation.withSecond(0).withNano(0);
        boolean areDateEquals = dateReservation.isEqual(dateReservationDispo);
        boolean isReservationAfterBeginingDispo = dateReservation.isAfter(dateReservationDispo);
        boolean isReservationBeforeEndDispo = dateReservation.isBefore(dateReservationDispo.plusHours(1));
        return areDateEquals || ( isReservationAfterBeginingDispo && isReservationBeforeEndDispo);
    }

    private List<ReservationParCourtVue> getReservationParCourtVueList(DispoReservationDto dispoReservationDto) {
        ReglesClubVue reglesClubVue = utilsTools.restCall(baseUrlReglesClubGet, HttpMethod.GET, new ParameterizedTypeReference<ReglesClubVue>() {});
        LocalTime heureDebut;
        if (dispoReservationDto.getDateReservation().equals(LocalDate.now())) {
            LocalTime heureDebutClub = reglesClubVue.getHoraireDebutAcces();
            LocalTime heureActuelle = LocalTime.now().withSecond(0).withNano(0);
            if(heureActuelle.isBefore(heureDebutClub) || heureActuelle.equals(heureDebutClub)) {
                heureDebut = heureDebutClub;
            } else {
                if(heureActuelle.getMinute() > heureDebutClub.getMinute()) {
                    heureActuelle = heureActuelle.plusHours(1);
                }
                heureDebut = heureActuelle.withMinute(heureDebutClub.getMinute());
            }
        } else {
            heureDebut = reglesClubVue.getHoraireDebutAcces();
        }
        LocalTime heureFin = reglesClubVue.getHoraireFinOuverture();
        if(heureDebut.until(heureFin, MINUTES) < (60*reservationDurationInHour)) {
            return Collections.emptyList();
        }
        List<CourtVue> courtList = getCourtList(dispoReservationDto);
        courtList.sort(Comparator.comparing(CourtVue::getIdCourt));
        List<ReservationVue> reservationVueList;
        List<ReservationParCourtVue> reservationParCourtVueList = new ArrayList<>();
        int nbReservation = getNumberOfPossibleReservation(heureDebut, heureFin);
        LocalDateTime dateTimeReservation = changeHourOfDateFromOtherDate(dispoReservationDto.getDateReservation(), heureDebut);
        int index;
        ReservationVue reservationVue;
        for(CourtVue courtVue : courtList) {
            reservationVueList = new ArrayList<>();
            for(index = 0 ; index < nbReservation ; index++) {
                reservationVue = new ReservationVue();
                reservationVue.setIdCourt(courtVue);
                reservationVue.setDateDebutReservation(dateTimeReservation.plusHours(index*reservationDurationInHour));
                reservationVueList.add(reservationVue);
            }
            reservationParCourtVueList.add(new ReservationParCourtVue(courtVue.getIdCourt(), reservationVueList));
        }
        return reservationParCourtVueList;
    }

    public List<ReservationParCourtVue> getPlanningReservation(LocalDate dateReservation, Integer idTypeCourt, Boolean estCouvert, String libelleCourt, Integer idCourt) throws ParameterException {
        DispoReservationDto dispoReservationDto = new DispoReservationDto(dateReservation, idTypeCourt, estCouvert, libelleCourt, idCourt);
        if(dispoReservationDto.getDateReservation() == null) {
            throw new ParameterException();
        }

        List<ReservationParCourtVue>  reservationParCourtVueList = getReservationParCourtVueList(dispoReservationDto);
        ReservationVue reservationVue;
        Optional<List<Reservation>> reservationListOptional = getReservationList(dispoReservationDto);
        if(reservationListOptional.isPresent() && !reservationParCourtVueList.isEmpty()) {
            int indexReservationParCourtVueList = 0, indexReservDispo;
            boolean reservationTrouvee;
            ReservationParCourtVue reservationParCourtVue;
            for(Reservation reservation : reservationListOptional.get()) {
                reservationTrouvee = false;
                for( ; indexReservationParCourtVueList < reservationParCourtVueList.size() ; indexReservationParCourtVueList++) {
                    reservationParCourtVue = reservationParCourtVueList.get(indexReservationParCourtVueList);
                    //Si l'id du court de la reservation de correspond pas à celui présent dans la dispo on passe aux dispo du court suivant
                    if(!reservation.getIdCourt().equals(reservationParCourtVue.getIdCourt()))
                        continue;
                    //Parcours toutes les dispo pour voir laquelle/lesquelles correspond(ent) à la reservation puis suppression de cette dispo
                    for(indexReservDispo = 0 ; indexReservDispo < reservationParCourtVue.getReservationVueList().size() ; indexReservDispo++) {
                        //for(ReservationVue reservationVue1 : reservationParCourtVue.getReservationVueList()) {
                        reservationVue = reservationParCourtVue.getReservationVueList().get(indexReservDispo);
                        //Si dispo correspond à la reservation : remplace pas la vraie reservation
                        if(isTheSameReservation(reservationVue.getDateDebutReservation(), reservation.getDateDebutReservation())) {
                            if(reservationTrouvee) {
                                reservationParCourtVue.getReservationVueList().remove(reservationVue);
                                indexReservDispo--;
                            } else {
                                reservationParCourtVue.getReservationVueList().set(indexReservDispo, modelMapper.map(reservation, ReservationVue.class));
                                reservationTrouvee = true;
                            }
                            continue;
                        }
                        //Suppression effectuée : on rompt la boucle de parcourt des dispo pour ce court
                        if(reservationTrouvee)
                            break;
                    }
                    //Suppression effectuée : on rompt la boucle de parcourt par court pour passer à la reservation suivante
                    if(reservationTrouvee)
                        break;
                }
            }
        }
        return reservationParCourtVueList;
    }

    public List<ReservationParCourtVue> getDispoReservation(LocalDate dateReservation, Integer idTypeCourt, Boolean estCouvert, String libelleCourt, Integer idCourt) throws ParameterException {
        DispoReservationDto dispoReservationDto = new DispoReservationDto(dateReservation, idTypeCourt, estCouvert, libelleCourt, idCourt);
        if(dispoReservationDto.getDateReservation() == null) {
            throw new ParameterException();
        }

        List<ReservationParCourtVue>  reservationParCourtVueList = getReservationParCourtVueList(dispoReservationDto);
        ReservationVue reservationVue;
        Optional<List<Reservation>> reservationListOptional = getReservationList(dispoReservationDto);
        if(reservationListOptional.isPresent() && !reservationParCourtVueList.isEmpty()) {
            int indexReservationParCourtVueList = 0, indexReservDispo;
            boolean reservationTrouvee;
            ReservationParCourtVue reservationParCourtVue;
            for(Reservation reservation : reservationListOptional.get()) {
                reservationTrouvee = false;
                for( ; indexReservationParCourtVueList < reservationParCourtVueList.size() ; indexReservationParCourtVueList++) {
                    reservationParCourtVue = reservationParCourtVueList.get(indexReservationParCourtVueList);
                    //Si l'id du court de la reservation de correspond pas à celui présent dans la dispo on passe aux dispo du court suivant
                    if(!reservation.getIdCourt().equals(reservationParCourtVue.getIdCourt()))
                        continue;
                    //Parcours toutes les dispo pour voir laquelle/lesquelles correspond(ent) à la reservation puis suppression de cette dispo
                    for(indexReservDispo = 0 ; indexReservDispo < reservationParCourtVue.getReservationVueList().size() ; indexReservDispo++) {
                    //for(ReservationVue reservationVue1 : reservationParCourtVue.getReservationVueList()) {
                        reservationVue = reservationParCourtVue.getReservationVueList().get(indexReservDispo);
                        //Si dispo correspond à la reservation : suppression de la dispo
                        if(isTheSameReservation(reservationVue.getDateDebutReservation(), reservation.getDateDebutReservation())) {
                            reservationParCourtVue.getReservationVueList().remove(reservationVue);
                            reservationTrouvee = true;
                            indexReservDispo--;
                            continue;
                        }
                        //Suppression effectuée : on rompt la boucle de parcourt des dispo pour ce court
                        if(reservationTrouvee)
                            break;
                    }
                    //Suppression effectuée : on rompt la boucle de parcourt par court pour passer à la reservation suivante
                    if(reservationTrouvee)
                        break;
                }
            }
        }
        return reservationParCourtVueList;
    }

    private List<CourtVue> getCourtList(DispoReservationDto dispoReservationDto) {
        Map<String, String> params;
        String urlCourtGetFromParam = baseUrlCourtGet+"byCKAndCover";
        if(dispoReservationDto.getLibelleCourt() != null || dispoReservationDto.getIdCourt() != null) {
            params = new HashMap<>();
            if(dispoReservationDto.getIdCourt() != null) {
                params.put("idCourt", dispoReservationDto.getIdCourt().toString());
            } else {
                params.put("libelleCourt", dispoReservationDto.getLibelleCourt());
            }
            return utilsTools.restCall(urlCourtGetFromParam, params, HttpMethod.GET, new ParameterizedTypeReference<List<CourtVue>>() {});
        } else {
            if (dispoReservationDto.getEstCouvert() != null && dispoReservationDto.getIdTypeCourt() != null) {
                params = new HashMap<>();
                params.put("isCovered", dispoReservationDto.getEstCouvert().toString());
                params.put("idCourtType", dispoReservationDto.getIdTypeCourt().toString());
                return utilsTools.restCall(urlCourtGetFromParam, params, HttpMethod.GET, new ParameterizedTypeReference<List<CourtVue>>() {});
            } else if (dispoReservationDto.getEstCouvert() != null) {
                params = new HashMap<>();
                params.put("isCovered", dispoReservationDto.getEstCouvert().toString());
                return utilsTools.restCall(urlCourtGetFromParam, params, HttpMethod.GET, new ParameterizedTypeReference<List<CourtVue>>() {});
            } else if (dispoReservationDto.getIdTypeCourt() != null) {
                params = new HashMap<>();
                String test = dispoReservationDto.getIdTypeCourt().toString();
                params.put("idCourtType", dispoReservationDto.getIdTypeCourt().toString());
                return utilsTools.restCall(urlCourtGetFromParam, params, HttpMethod.GET, new ParameterizedTypeReference<List<CourtVue>>() {});
            } else {
                return utilsTools.restCall(baseUrlCourtGet, HttpMethod.GET, new ParameterizedTypeReference<List<CourtVue>>() {});
            }
        }
    }

    private Optional<List<Reservation>> getReservationList(DispoReservationDto dispoReservationDto) {
        LocalDateTime dateDebutJourneeReservation = dispoReservationDto.getDateReservation().atStartOfDay();
        LocalDateTime dateFinJourneeReservation = dispoReservationDto.getDateReservation().plusDays(1).atStartOfDay();
        List<CourtVue> courtList = getCourtList(dispoReservationDto);
        List<Integer> idCourtList;
        Optional<List<Reservation>> reservationList = Optional.empty();
        if (!courtList.isEmpty()) {
            idCourtList = new ArrayList<>();
            for(CourtVue courtVue : courtList) {
                idCourtList.add(courtVue.getIdCourt());
            }
            reservationList = reservationRepository.findAllByDateDebutReservationAndCourtIdList(dateDebutJourneeReservation, dateFinJourneeReservation, idCourtList);
            reservationList.ifPresent(reservations -> reservations.sort(Comparator.comparing(Reservation::getIdCourt).thenComparing(Reservation::getDateDebutReservation)));;
        }
        return reservationList;
    }

    public List<ReservationParCourtVue> getReservationByCourt(LocalDate dateReservation, Integer idTypeCourt, Boolean estCouvert, String libelleCourt, Integer idCourt) throws ParameterException {
        DispoReservationDto dispoReservationDto = new DispoReservationDto(dateReservation, idTypeCourt, estCouvert, libelleCourt, idCourt);
        if(dispoReservationDto.getDateReservation() == null) {
            throw new ParameterException();
        }

        Optional<List<Reservation>> reservationList = getReservationList(dispoReservationDto);

        if(reservationList.isPresent()) {
            Type listType = new TypeToken<List<ReservationVue>>() {}.getType();
            List<ReservationVue> reservationVueList = modelMapper.map(reservationList.get(), listType);

            List<ReservationParCourtVue> reservationParCourtVueList = new ArrayList<>();
            ReservationParCourtVue derniereRPCVAjoutee;

            for (ReservationVue reservationVue : reservationVueList) {
                if(!reservationParCourtVueList.isEmpty()) {
                    derniereRPCVAjoutee = reservationParCourtVueList.get(reservationParCourtVueList.size() - 1);
                    if(!derniereRPCVAjoutee.getIdCourt().equals(reservationVue.getIdCourt().getIdCourt())) {
                        reservationParCourtVueList.add(new ReservationParCourtVue(reservationVue.getIdCourt().getIdCourt(), new ArrayList<>(Collections.singletonList(reservationVue))));
                    }
                    else {
                        derniereRPCVAjoutee.getReservationVueList().add(reservationVue);
                    }
                } else {
                    reservationParCourtVueList.add(new ReservationParCourtVue(reservationVue.getIdCourt().getIdCourt(), new ArrayList<>(Collections.singletonList(reservationVue))));
                }
            }

            return reservationParCourtVueList;
        } else {
            return Collections.<ReservationParCourtVue>emptyList();
        }
    }

    private boolean isReservationWithinAllowedHours(LocalDateTime reservationDate) {
        ReglesClubVue reglesClubVue = utilsTools.restCall(baseUrlReglesClubGet, HttpMethod.GET, new ParameterizedTypeReference<ReglesClubVue>() {});
        LocalTime reservationTime = reservationDate.toLocalTime();
        boolean areReservationMinEqStartMin = reservationTime.getMinute() == reglesClubVue.getHoraireDebutAcces().getMinute();
        boolean isReservationBeginRightHour = reservationTime.equals(reglesClubVue.getHoraireDebutAcces()) || reservationTime.isAfter(reglesClubVue.getHoraireDebutAcces());
        boolean isReservationEndBeforeRightHour = reservationTime.plusHours(reservationDurationInHour).equals(reglesClubVue.getHoraireFinOuverture()) ||
                reservationTime.plusHours(reservationDurationInHour).isBefore(reglesClubVue.getHoraireFinOuverture());
        return areReservationMinEqStartMin && isReservationBeginRightHour && isReservationEndBeforeRightHour;
    }

    public ReservationVue createReservation(ReservationDto reservationDto) throws ParameterException, NoReservationPossibleException {
        if (reservationDto == null) {
            throw new ParameterException();
        }

        Reservation reservationModel = modelMapper.map(reservationDto, Reservation.class);
        if(!isReservationWithinAllowedHours(reservationDto.getDateDebutReservation())) {
            throw new NoReservationPossibleException();
        }
        if(!doUsersAndCourtExists(reservationModel) || !isReservationPossibleForPlayers(reservationDto)) {
            throw new NoReservationPossibleException();
        }
        reservationModel.setIdReservation(null); //TODO: comprendre pourquoi l'id commence à 1 alors qu'il y a déjà 2 insertions (1 et 2)
        reservationModel = reservationRepository.save(reservationModel);

        return  modelMapper.map(reservationModel, ReservationVue.class);
    }

    @Override
    public ReservationVue createReservationByUser(ReservationDto reservationDto, UtilisateurVue utilisateurVue) throws ParameterException, NoReservationPossibleException {
        if (reservationDto == null) {
            throw new ParameterException();
        }

        reservationDto.setIdUtilisateur2(reservationDto.getIdUtilisateur1());
        reservationDto.setIdUtilisateur1(utilisateurVue.getIdUtilisateur());

        return this.createReservation(reservationDto);
    }

    private boolean doUsersAndCourtExists(Reservation reservation) {
        if(reservation.getIdUtilisateur1() == null){
            return false;
        }

        UtilisateurVue utilisateurVue1 = utilsTools.restCall(baseUrlUtilisateurGet + reservation.getIdUtilisateur1(), HttpMethod.GET, new ParameterizedTypeReference<UtilisateurVue>() {});

        UtilisateurVue utilisateurVue2 = null;
        if(reservation.getIdUtilisateur2() != null) {
            utilisateurVue2 = utilsTools.restCall(baseUrlUtilisateurGet + reservation.getIdUtilisateur1(), HttpMethod.GET, new ParameterizedTypeReference<UtilisateurVue>() {});
        }

        CourtDto courtDto = utilsTools.restCall(baseUrlCourtGet + reservation.getIdCourt(), HttpMethod.GET, new ParameterizedTypeReference<CourtDto>() {});

        //Check if users and court exist
        if (utilisateurVue1 == null ||
                (reservation.getIdUtilisateur2() != null && utilisateurVue2 == null) ||
                courtDto == null) {
            return false;
        }

        //Check if user1 and user2 are not the same player
        if(reservation.getIdUtilisateur1().equals(reservation.getIdUtilisateur2())) {
            return false;
        }

        return true;
    }

    private boolean isReservationPossibleForPlayers(ReservationDto reservation) {
        //Check if there is already a reservation at this time on the court
        if(isAlreadyReserved(reservation.getIdCourt(), reservation.getDateDebutReservation())) {
            return false;
        }

        //Check if both users doesn't have a reservation at this time (on another court)
        if(isPlayerAlreadyPlayingSpecificDate(reservation.getIdUtilisateur1(), reservation.getDateDebutReservation())) {
            return false;
        } else if(reservation.getIdUtilisateur2() != null ) {
            if(isPlayerAlreadyPlayingSpecificDate(reservation.getIdUtilisateur2(), reservation.getDateDebutReservation())) {
                return false;
            }
        }
        return true;
    }

    private boolean isAlreadyReserved(Integer idCourt, LocalDateTime dateReservation) {
        Reservation reservation = reservationRepository.findByIdCourtAndDateDebutReservation(idCourt, dateReservation);
        return reservation != null;
    }

    private boolean isPlayerAlreadyPlayingSpecificDate(Integer idUser, LocalDateTime dateReservation) {
        Reservation reservation = reservationRepository.findByIdUserAndDateReservation(idUser, dateReservation);
        return reservation != null;
    }

    public Void removeReservation(Integer id) throws NotFoundException {
        Optional<Reservation> optReservation = reservationRepository.findById(id);
        if (!optReservation.isPresent()) {
            throw new NotFoundException("Reservation " + id);
        }
        reservationRepository.delete(optReservation.get());
        return null;
    }

    public ReservationVue updateReservation(Integer id, ReservationDto reservationDto) throws ParameterException, UserCourtNotExistException, NotFoundException {
        if (reservationDto == null) {
            throw new ParameterException();
        }

        if(reservationDto.getDateDebutReservation() != null && !isReservationWithinAllowedHours(reservationDto.getDateDebutReservation())) {
            throw new NoReservationPossibleException();
        }

        //Check if there is an original reservation
        Optional<Reservation> optExistingReserv = reservationRepository.findById(id);
        if (!optExistingReserv.isPresent()) {
            throw new NotFoundException("Reservation " + id);
        }
        ReservationDto existingReservationDto = modelMapper.map(optExistingReserv.get(), ReservationDto.class);

        Reservation reservationModel = modelMapper.map(reservationDto, Reservation.class);
        UtilsTools.copyNonNullProperties(reservationModel, optExistingReserv.get());

        Reservation mergedReservation = optExistingReserv.get();

        //Check if users and court exist
        if(!doUsersAndCourtExists(mergedReservation)) {
            throw new UserCourtNotExistException();
        }

        if(reservationDto.getIdUtilisateur1() != null) {
            //if the user1 is different than the one in the original reservation, check if he's not already playing at this time
            if (!existingReservationDto.getIdUtilisateur1().equals(reservationDto.getIdUtilisateur1())) {
                if (isPlayerAlreadyPlayingSpecificDate(reservationDto.getIdUtilisateur1(), reservationDto.getDateDebutReservation())) {
                    //return ResponseEntity.badRequest().body("Le joueur d'id " + reservation.getIdUtilisateur1() + " a déjà une reservation à cet horaire");
                    throw new NoReservationPossibleException();
                }
            }
        }

        //if the user2 is different than the one in the original reservation, check if he's not already playing at this time
        if(reservationDto.getIdUtilisateur2() != null) {
            if (!reservationDto.getIdUtilisateur2().equals(existingReservationDto.getIdUtilisateur2())) {
                if (isPlayerAlreadyPlayingSpecificDate(reservationDto.getIdUtilisateur2(), reservationDto.getDateDebutReservation())) {
                    //return ResponseEntity.badRequest().body("Le joueur d'id " + reservation.getIdUtilisateur2() + " a déjà une reservation à cet horaire");
                    throw new NoReservationPossibleException();
                }
            }
        }


        //check if court has changed
        boolean isCourtDifferent = false;
        Integer idCourt = existingReservationDto.getIdCourt();
        if(reservationDto.getIdCourt() != null && !reservationDto.getIdCourt().equals(idCourt)) {
            isCourtDifferent = true;
            idCourt = reservationDto.getIdCourt();
        }
        //check if reservation date has changed
        boolean isStartDateDifferent = false;
        LocalDateTime dateReservation = existingReservationDto.getDateDebutReservation();
        if(reservationDto.getDateDebutReservation() != null && !reservationDto.getDateDebutReservation().equals(dateReservation)) {
            isStartDateDifferent = true;
            dateReservation = reservationDto.getDateDebutReservation();
        }
        //if the court or/and the date changed, then we check if the court at the reservation date is available
        if(isCourtDifferent || isStartDateDifferent) {
            if(isAlreadyReserved(idCourt, dateReservation)) {
                //return ResponseEntity.badRequest().body("Le terrain d'id " + id + " est déjà réservé à l'horaire : " + reservation.getDateDebutReservation());
                throw new NoReservationPossibleException();
            }
        }


        reservationModel = reservationRepository.save(mergedReservation);

        return modelMapper.map(reservationModel, ReservationVue.class);
    }

    @Override
    public List<ReservationVue> getReservationByUser(UtilisateurVue utilisateurVue) throws InternalServerException {
        if (utilisateurVue == null) {
            throw new InternalServerException();
        }

        Optional<List<Reservation>> optionalReservationList = reservationRepository.findByIdUtilisateur(utilisateurVue.getIdUtilisateur());
        if(optionalReservationList.isPresent() && !optionalReservationList.get().isEmpty()) {
            optionalReservationList.get().sort(Comparator.comparing(Reservation::getDateDebutReservation).reversed());

            Type listType = new TypeToken<List<ReservationVue>>() {}.getType();
            return modelMapper.map(optionalReservationList.get(), listType);
        }
        return Collections.emptyList();
    }

    public Void removeReservationByUser(Integer id, UtilisateurVue utilisateurVue) throws NotFoundException {
        Optional<Reservation> optReservation = reservationRepository.findById(id);
        if (!optReservation.isPresent()) {
            throw new NotFoundException("Reservation " + id);
        }
        if(!optReservation.get().getIdUtilisateur1().equals(utilisateurVue.getIdUtilisateur()) &&
                !optReservation.get().getIdUtilisateur2().equals(utilisateurVue.getIdUtilisateur())) {
            throw new NotFoundException("Reservation " + id + " for user id " + utilisateurVue.getIdUtilisateur());
        }
        reservationRepository.delete(optReservation.get());
        return null;
    }

    @Override
    public List<ReservationVue> getReservationByUserId(Integer utilisateurId) {
        Optional<List<Reservation>> optionalReservationList = reservationRepository.findByIdUtilisateur(utilisateurId);
        if(optionalReservationList.isPresent() && !optionalReservationList.get().isEmpty()) {
            Type listType = new TypeToken<List<ReservationVue>>() {}.getType();
            return modelMapper.map(optionalReservationList.get(), listType);
        }
        return Collections.emptyList();
    }
}
