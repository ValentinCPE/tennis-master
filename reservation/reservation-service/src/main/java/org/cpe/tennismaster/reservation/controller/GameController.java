package org.cpe.tennismaster.reservation.controller;

import lombok.extern.slf4j.Slf4j;
import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.reservation.common.dto.PartieDto;
import org.cpe.tennismaster.reservation.common.vue.PartieVue;
import org.cpe.tennismaster.reservation.model.Partie;
import org.cpe.tennismaster.reservation.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/game")
public class GameController {

    @Autowired
    private GameService gameService;

    @GetMapping
    public List<PartieVue> getAllGame() {
        return gameService.getAllGame();
    }

    @GetMapping("/{id}")
    public ResponseEntity<PartieVue> getGameById(@PathVariable Integer id) throws NotFoundException {
        return ResponseEntity.ok(gameService.getGameById(id));
    }

    @PostMapping
    public ResponseEntity<PartieVue> createGame(@RequestBody PartieDto partieDto) throws ParameterException {
        return ResponseEntity.status(HttpStatus.CREATED).body(gameService.createGame(partieDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> removeGame(@PathVariable Integer id) throws NotFoundException {
        return ResponseEntity.ok(gameService.removeGame(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PartieVue> updateGame(@PathVariable Integer id, @RequestBody PartieDto partieDto) throws ParameterException {
        return ResponseEntity.ok(gameService.updateGame(id, partieDto));
    }

}
