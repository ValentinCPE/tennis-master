package org.cpe.tennismaster.reservation.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "partie")
public class Partie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, name = "id_partie")
    private Integer idPartie;

    @NotNull
    @Column(name = "score_j_1")
    private String scoreJ1; //TODO: voir avec équipe pour changer format score

    @NotNull
    @Column(name = "score_j_2")
    private String scoreJ2;

    @OneToOne
    @JoinColumn(name = "id_reservation", referencedColumnName = "id_reservation")
    @JsonBackReference
    private Reservation reservation;

    public Partie() {
    }

    public Partie(String scoreJ1, String scoreJ2) {
        this.scoreJ1 = scoreJ1;
        this.scoreJ2 = scoreJ2;
    }

    public Integer getIdPartie() {
        return idPartie;
    }

    public void setIdPartie(Integer idPartie) {
        this.idPartie = idPartie;
    }

    public String getScoreJ1() {
        return scoreJ1;
    }

    public void setScoreJ1(String scoreJ1) {
        this.scoreJ1 = scoreJ1;
    }

    public String getScoreJ2() {
        return scoreJ2;
    }

    public void setScoreJ2(String scoreJ2) {
        this.scoreJ2 = scoreJ2;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }
}
