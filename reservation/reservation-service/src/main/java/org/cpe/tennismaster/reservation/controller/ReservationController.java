package org.cpe.tennismaster.reservation.controller;

import lombok.extern.slf4j.Slf4j;

import org.cpe.tennismaster.common.exception.InternalServerException;
import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.common.model.utilisateur.UtilisateurVue;
import org.cpe.tennismaster.common.roles.Roles;
import org.cpe.tennismaster.reservation.common.dto.ReservationDto;
import org.cpe.tennismaster.reservation.common.exception.NoReservationPossibleException;
import org.cpe.tennismaster.reservation.common.exception.UserCourtNotExistException;
import org.cpe.tennismaster.reservation.common.vue.ReservationGrantedVue;
import org.cpe.tennismaster.reservation.common.vue.ReservationParCourtVue;
import org.cpe.tennismaster.reservation.common.vue.ReservationVue;
import org.cpe.tennismaster.reservation.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.*;

@Slf4j
@RestController
@RequestMapping("/reservations")
public class ReservationController {

    private final ReservationService reservationService;

    @Autowired
    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @GetMapping
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<List<ReservationVue>> getAllReservations() {
        return ResponseEntity.ok(reservationService.getAllReservations());
    }

    @GetMapping("/validateToken")
    public ResponseEntity<ReservationGrantedVue> tokenReservationAutorise(@RequestParam(name = "code") String code) throws InternalServerException {
        return ResponseEntity.ok(reservationService.tokenReservationAutorise(code));
    }

    @GetMapping("/week")
    @PreAuthorize("hasAnyRole('"+Roles.Libelle.USER + "','"+Roles.Libelle.ADMIN + "')")
    public ResponseEntity<List<ReservationVue>> getReservationsByWeek(@RequestParam(name = "date") String dateString) throws InternalServerException {
        return ResponseEntity.ok(reservationService.getReservationsByWeek(dateString));
    }

    @GetMapping("/user/{id}")
    @PreAuthorize("hasRole('"+ Roles.Libelle.USER +"')")
    public ResponseEntity<List<ReservationVue>> getReservationsByWeek(@PathVariable Integer id) throws InternalServerException {
        return ResponseEntity.ok(reservationService.getReservationsByWeek(id));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('"+Roles.Libelle.USER + "','"+Roles.Libelle.ADMIN + "')")
    public ResponseEntity<ReservationVue> getReservationById(@PathVariable Integer id) throws NotFoundException {
        return ResponseEntity.ok(reservationService.getReservationById(id));
    }

    @GetMapping("/planning")
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<List<ReservationParCourtVue>> getPlanningReservation(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateReservation, @RequestParam(required = false) Integer idTypeCourt,
                                                                            @RequestParam(required = false) Boolean estCouvert, @RequestParam(required = false) String libelleCourt,
                                                                            @RequestParam(required = false) Integer idCourt) throws NotFoundException {
        return ResponseEntity.ok(reservationService.getPlanningReservation(dateReservation, idTypeCourt, estCouvert, libelleCourt, idCourt));
    }

    @GetMapping("/dispo")
    @PreAuthorize("hasAnyRole('"+Roles.Libelle.USER + "','"+Roles.Libelle.ADMIN + "')")
    public ResponseEntity<List<ReservationParCourtVue>> getDispoReservation(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateReservation, @RequestParam(required = false) Integer idTypeCourt,
                                                                            @RequestParam(required = false) Boolean estCouvert, @RequestParam(required = false) String libelleCourt,
                                                                            @RequestParam(required = false) Integer idCourt) throws NotFoundException {
        return ResponseEntity.ok(reservationService.getDispoReservation(dateReservation, idTypeCourt, estCouvert, libelleCourt, idCourt));
    }

    @GetMapping("/byCourt")
    @PreAuthorize("hasAnyRole('"+Roles.Libelle.USER + "','"+Roles.Libelle.ADMIN + "')")
    public ResponseEntity<List<ReservationParCourtVue>> getReservationByCourt(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateReservation, @RequestParam(required = false) Integer idTypeCourt,
                                                                              @RequestParam(required = false) Boolean estCouvert, @RequestParam(required = false) String libelleCourt,
                                                                              @RequestParam(required = false) Integer idCourt) throws NotFoundException {
        return ResponseEntity.ok(reservationService.getReservationByCourt(dateReservation, idTypeCourt, estCouvert, libelleCourt, idCourt));
    }

    @GetMapping("/byUserId")
    @PreAuthorize("hasRole('"+Roles.Libelle.ADMIN + "')")
    public ResponseEntity<List<ReservationVue>> getReservationByUserId(@RequestParam("userId") Integer utilisateurId) {
        return ResponseEntity.ok(reservationService.getReservationByUserId(utilisateurId));
    }

    @PostMapping
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<ReservationVue> createReservation(@RequestBody ReservationDto reservationDto) throws ParameterException, NoReservationPossibleException {
        return ResponseEntity.status(HttpStatus.CREATED).body(reservationService.createReservation(reservationDto));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<Void> removeReservation(@PathVariable Integer id) throws NotFoundException {
        return ResponseEntity.ok(reservationService.removeReservation(id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<ReservationVue> updateReservation(@PathVariable Integer id, @RequestBody ReservationDto reservationDto) throws ParameterException, UserCourtNotExistException, NotFoundException {
        return ResponseEntity.ok(reservationService.updateReservation(id, reservationDto));
    }

    @GetMapping("/me")
    @PreAuthorize("hasAnyRole('"+Roles.Libelle.USER + "','"+Roles.Libelle.ADMIN + "')")
    public ResponseEntity<List<ReservationVue>> getReservationByUser(@RequestAttribute("user") UtilisateurVue utilisateurVue) {
        return ResponseEntity.ok(reservationService.getReservationByUser(utilisateurVue));
    }

    @PostMapping(value = "/user")
    @PreAuthorize("hasRole('"+ Roles.Libelle.USER +"')")
    public ResponseEntity<ReservationVue> createReservationByUser(@RequestBody ReservationDto reservationDto, @RequestAttribute("user") UtilisateurVue utilisateurVue) throws ParameterException, NoReservationPossibleException {
        return ResponseEntity.status(HttpStatus.CREATED).body(reservationService.createReservationByUser(reservationDto, utilisateurVue));
    }

    @DeleteMapping("/me/{id}")
    @PreAuthorize("hasAnyRole('"+Roles.Libelle.USER + "','"+Roles.Libelle.ADMIN + "')")
    public ResponseEntity<Void> removeReservationByUser(@PathVariable Integer id, @RequestAttribute("user") UtilisateurVue utilisateurVue) throws NotFoundException {
        return ResponseEntity.ok(reservationService.removeReservationByUser(id, utilisateurVue));
    }

}
