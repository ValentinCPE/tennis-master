package org.cpe.tennismaster.reservation.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@ToString
public class ReservationUserDto {

    @NotNull
    private Integer idUtilisateur1;

    private Integer idUtilisateur2;

    @NotNull
    private Integer idCourt;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date dateDebutReservation;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date dateCreationReservation;

    private Boolean valide;

    @JsonManagedReference
    private PartieDto partie;

    public ReservationUserDto() {
    }

    public ReservationUserDto(Integer idUtilisateur1, Integer idUtilisateur2, Integer idCourt, Date dateDebutReservation, Date dateCreationReservation, Boolean valide, PartieDto partie) {
        this.idUtilisateur1 = idUtilisateur1;
        this.idUtilisateur2 = idUtilisateur2;
        this.idCourt = idCourt;
        this.dateDebutReservation = dateDebutReservation;
        this.dateCreationReservation = dateCreationReservation;
        this.valide = valide;
        this.partie = partie;
    }

    public Integer getIdUtilisateur1() {
        return idUtilisateur1;
    }

    public void setIdUtilisateur1(Integer idUser) {
        this.idUtilisateur1 = idUser;
        //this.userOne.addReservationPlayer1(this);
    }

    public Integer getIdUtilisateur2() {
        return idUtilisateur2;
    }

    public void setIdUtilisateur2(Integer idUser) {
        this.idUtilisateur2 = idUser;
        //this.idUser2.addReservationPlayer2(this);
    }

    public Integer getIdCourt() {
        return idCourt;
    }

    public void setIdCourt(Integer idCourt) {
        this.idCourt = idCourt;
        //this.court.addReservation(this);
    }

    public Date getDateDebutReservation() {
        return dateDebutReservation;
    }

    public void setDateDebutReservation(Date dateDebutReservation) {
        this.dateDebutReservation = dateDebutReservation;
    }

    public Date getDateCreationReservation() {
        return dateCreationReservation;
    }

    public void setDateCreationReservation(Date dateCreationReservation) {
        this.dateCreationReservation = dateCreationReservation;
    }

    public Boolean getValide() {
        return valide;
    }

    public void setValide(Boolean valid) {
        valide = valid;
    }

    public PartieDto getPartie() {
        return partie;
    }

    public void setPartie(PartieDto partie) {
        this.partie = partie;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                //"\n\tuserOne=" + (this.userOne == null ? "nulll" : userOne.getFirstName()) +
                //"\n\tidUser2=" + (this.idUser2 == null ? "nulll" : idUser2.getFirstName()) +
                "\n\tcourt=" + (this.idCourt == null ? "nulll" : idCourt) +
                "\n\tdateReservation=" + (this.dateDebutReservation == null ? "nulll" : dateDebutReservation) +
                "\n\tdateCreationReservation=" + (this.dateCreationReservation == null ? "nulll" : dateCreationReservation) +
                '}';
    }

}
