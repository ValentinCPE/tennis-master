package org.cpe.tennismaster.reservation.common.exception;

import org.cpe.tennismaster.common.exception.GeneralException;
import org.springframework.http.HttpStatus;

public class NoReservationPossibleException extends GeneralException {

    public NoReservationPossibleException(){
        super();
    }

    @Override
    protected HttpStatus defineStatusHttp() {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    protected String defineErrorMessage(String variable) {
        return "Impossible de réserver.";
    }
}
