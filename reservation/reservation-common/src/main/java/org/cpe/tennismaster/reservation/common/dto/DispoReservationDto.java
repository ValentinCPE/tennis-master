package org.cpe.tennismaster.reservation.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class DispoReservationDto {

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dateReservation;

    private Integer idTypeCourt;

    private Boolean estCouvert;

    private String libelleCourt;

    private Integer idCourt;

    public DispoReservationDto() {

    }

    public DispoReservationDto(@NotNull LocalDate dateReservation, Integer idTypeCourt, Boolean estCouvert, String libelleCourt, Integer idCourt) {
        this.dateReservation = dateReservation;
        this.idTypeCourt = idTypeCourt;
        this.estCouvert = estCouvert;
        this.libelleCourt = libelleCourt;
        this.idCourt = idCourt;
    }

    public LocalDate getDateReservation() {
        return dateReservation;
    }

    public void setDateReservation(LocalDate dateReservation) {
        this.dateReservation = dateReservation;
    }

    public Integer getIdTypeCourt() {
        return idTypeCourt;
    }

    public void setIdTypeCourt(Integer idTypeCourt) {
        this.idTypeCourt = idTypeCourt;
    }

    public Boolean getEstCouvert() {
        return estCouvert;
    }

    public void setEstCouvert(Boolean estCouvert) {
        this.estCouvert = estCouvert;
    }

    public String getLibelleCourt() {
        return libelleCourt;
    }

    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    public Integer getIdCourt() {
        return idCourt;
    }

    public void setIdCourt(Integer idCourt) {
        this.idCourt = idCourt;
    }
}
