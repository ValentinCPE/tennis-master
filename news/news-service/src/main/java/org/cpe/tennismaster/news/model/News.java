package org.cpe.tennismaster.news.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "news")
@JsonIgnoreProperties(ignoreUnknown = true)
public class News {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, name = "id_news")
    private Integer idNews;

    @Column(name = "titre_news")
    @NotNull
    private String titreNews;

    @Column(name = "contenu_news")
    @NotNull
    private String contenuNews;

    @Column(name = "date_creation_news")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime dateCreationNews;

    @Column(name = "image_url")
    private String imageUrl;

    public News() {
    }

    @PrePersist
    void PreInsert() {
        this.dateCreationNews = LocalDateTime.now();
    }

    public Integer getIdNews() {
        return idNews;
    }

    public void setIdNews(Integer idNews) {
        this.idNews = idNews;
    }

    public String getTitreNews() {
        return titreNews;
    }

    public void setTitreNews(String titreNews) {
        this.titreNews = titreNews;
    }

    public String getContenuNews() {
        return contenuNews;
    }

    public void setContenuNews(String contenuNews) {
        this.contenuNews = contenuNews;
    }

    public LocalDateTime getDateCreationNews() {
        return dateCreationNews;
    }

    public void setDateCreationNews(LocalDateTime dateCreationNews) {
        this.dateCreationNews = dateCreationNews;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
