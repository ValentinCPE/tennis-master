package org.cpe.tennismaster.news.service;

import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.news.common.dto.NewsDto;
import org.cpe.tennismaster.news.common.vue.NewsVue;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface NewsService {


    List<NewsVue> getAllNews();

    NewsVue getNewsById(Integer id) throws ParameterException;

    NewsVue createNews(NewsDto newsDto) throws NotFoundException;

    Void deleteNews(Integer id) throws NotFoundException;

    NewsVue updateNews(Integer id, NewsDto newsDto) throws NotFoundException;
}
