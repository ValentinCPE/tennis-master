package org.cpe.tennismaster.news.common.vue;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NewsVue {

    private Integer idNews;

    @NotNull
    private String titreNews;

    @NotNull
    private String contenuNews;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime dateCreationNews;

    private String imageUrl;

    public NewsVue() {
    }

    public Integer getIdNews() {
        return idNews;
    }

    public void setIdNews(Integer idNews) {
        this.idNews = idNews;
    }

    public String getTitreNews() {
        return titreNews;
    }

    public void setTitreNews(String titreNews) {
        this.titreNews = titreNews;
    }

    public String getContenuNews() {
        return contenuNews;
    }

    public void setContenuNews(String contenuNews) {
        this.contenuNews = contenuNews;
    }

    public LocalDateTime getDateCreationNews() {
        return dateCreationNews;
    }

    public void setDateCreationNews(LocalDateTime dateCreationNews) {
        this.dateCreationNews = dateCreationNews;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
