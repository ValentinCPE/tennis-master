package org.cpe.tennismaster.reglesClub.controller;

import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.roles.Roles;
import org.cpe.tennismaster.reglesClub.common.dto.ReglesClubDto;
import org.cpe.tennismaster.reglesClub.common.vue.ReglesClubVue;
import org.cpe.tennismaster.reglesClub.service.ReglesClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/reglesClub")
public class ReglesClubController {

    private final ReglesClubService reglesClubService;

    @Autowired
    public ReglesClubController(ReglesClubService reglesClubService) {
        this.reglesClubService = reglesClubService;
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('"+Roles.Libelle.USER + "','"+Roles.Libelle.ADMIN + "')")
    public ResponseEntity<ReglesClubVue> getReglesClub() {
        return ResponseEntity.ok(reglesClubService.getReglesClub());
    }

    @PutMapping
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<ReglesClubVue> updateReglesClub(@RequestBody ReglesClubDto reglesClubDto) throws NotFoundException {
        return ResponseEntity.ok(reglesClubService.updateReglesClub(reglesClubDto));
    }

}
