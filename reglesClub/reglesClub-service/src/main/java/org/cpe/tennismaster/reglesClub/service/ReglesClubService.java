package org.cpe.tennismaster.reglesClub.service;

import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.reglesClub.common.dto.ReglesClubDto;
import org.cpe.tennismaster.reglesClub.common.vue.ReglesClubVue;
import org.springframework.stereotype.Service;

@Service
public interface ReglesClubService {

    ReglesClubVue getReglesClub();

    ReglesClubVue updateReglesClub(ReglesClubDto reglesClubDto) throws NotFoundException;

}
