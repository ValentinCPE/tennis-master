package org.cpe.tennismaster.reglesClub;

import org.cpe.tennismaster.common.CommonGeneralApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@Import(CommonGeneralApplication.class)
@SpringBootApplication
public class ReglesClubApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReglesClubApplication.class, args);
    }

}
