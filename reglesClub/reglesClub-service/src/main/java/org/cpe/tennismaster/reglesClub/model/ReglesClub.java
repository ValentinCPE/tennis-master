package org.cpe.tennismaster.reglesClub.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Table(name = "regles_club")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReglesClub {

    //TODO: limiter l'insertion dans regle club à 1
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, name = "id_regle")
    private Integer idRegle;

    private Integer nombreInvitation;

    @JsonFormat(pattern = "HH:mm")
    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    private LocalTime horaireDebutAcces;

    @JsonFormat(pattern = "HH:mm")
    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    private LocalTime  horaireFinOuverture;

    private Integer nbJourMaxReservation;

    @Column(length = 50)
    private String libelleRegle;

    public ReglesClub() {
    }

    @PrePersist
    void preInsert() {
        if (this.nbJourMaxReservation == null) {
            this.nbJourMaxReservation = 6;
        }
    }

    public Integer getIdRegle() {
        return idRegle;
    }

    public void setIdRegle(Integer idRegle) {
        this.idRegle = idRegle;
    }

    public Integer getNombreInvitation() {
        return nombreInvitation;
    }

    public void setNombreInvitation(Integer nombreInvitation) {
        this.nombreInvitation = nombreInvitation;
    }

    public LocalTime getHoraireDebutAcces() {
        return horaireDebutAcces;
    }

    public void setHoraireDebutAcces(LocalTime horaireDebutAcces) {
        this.horaireDebutAcces = horaireDebutAcces;
    }

    public LocalTime getHoraireFinOuverture() {
        return horaireFinOuverture;
    }

    public void setHoraireFinOuverture(LocalTime horaireFinOuverture) {
        this.horaireFinOuverture = horaireFinOuverture;
    }

    public Integer getNbJourMaxReservation() {
        return nbJourMaxReservation;
    }

    public void setNbJourMaxReservation(Integer nbJourMaxReservation) {
        this.nbJourMaxReservation = nbJourMaxReservation;
    }

    public String getLibelleRegle() {
        return libelleRegle;
    }

    public void setLibelleRegle(String libelleRegle) {
        this.libelleRegle = libelleRegle;
    }
}
