package org.cpe.tennismaster.reglesClub.repository;

import org.cpe.tennismaster.reglesClub.model.ReglesClub;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReglesClubRepository extends JpaRepository<ReglesClub, Integer> {

    List<ReglesClub> findAllBy();

}
