package org.cpe.tennismaster.reglesClub.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;

import java.time.LocalTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReglesClubDto {

    private Integer nombreInvitation;

    @JsonFormat(pattern = "HH:mm")
    @JsonDeserialize(using = LocalTimeDeserializer.class)
    private LocalTime  horaireDebutAcces;

    @JsonFormat(pattern = "HH:mm")
    @JsonDeserialize(using = LocalTimeDeserializer.class)
    private LocalTime horaireFinOuverture;

    private Integer nbJourMaxReservation;

    private String libelleRegle;

    public ReglesClubDto() {
    }

    public Integer getNombreInvitation() {
        return nombreInvitation;
    }

    public void setNombreInvitation(Integer nombreInvitation) {
        this.nombreInvitation = nombreInvitation;
    }

    public LocalTime getHoraireDebutAcces() {
        return horaireDebutAcces;
    }

    public void setHoraireDebutAcces(LocalTime horaireDebutAcces) {
        this.horaireDebutAcces = horaireDebutAcces;
    }

    public LocalTime getHoraireFinOuverture() {
        return horaireFinOuverture;
    }

    public void setHoraireFinOuverture(LocalTime horaireFinOuverture) {
        this.horaireFinOuverture = horaireFinOuverture;
    }

    public Integer getNbJourMaxReservation() {
        return nbJourMaxReservation;
    }

    public void setNbJourMaxReservation(Integer nbJourMaxReservation) {
        this.nbJourMaxReservation = nbJourMaxReservation;
    }

    public String getLibelleRegle() {
        return libelleRegle;
    }

    public void setLibelleRegle(String libelleRegle) {
        this.libelleRegle = libelleRegle;
    }
}
