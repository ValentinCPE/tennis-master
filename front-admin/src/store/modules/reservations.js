
const moduleReservation = {
  state: {
    all: [],
    reservationSelected: null
  },
  mutations: {
    setReservations (state, users) {
      state.all = users
    },
    setSelectedReservation (state, reservation) {
      state.reservationSelected = reservation
    }
  },
  actions: {
    setCurrentReservation ({commit}, data) {
      commit('setSelectedReservation', data)
    }
  },
  getters: {
  }
}

export default moduleReservation
