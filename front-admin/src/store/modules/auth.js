import router from '@/router'
import auth from '../../api/auth'
import jwtDecode from 'jwt-decode'

const moduleAuth = {
  state: {
    access_token: null,
    expToken: null,
    refresh_token: null
  },
  mutations: {
    login (state, userData) {
      let tokenJson = jwtDecode(userData.access_token)
      state.expToken = tokenJson.exp
      state.access_token = userData.access_token
      state.refresh_token = userData.refresh_token
    },
    logout (state) {
      state.access_token = null
      state.expToken = null
      state.refresh_token = null
    }
  },
  getters: {
    loggedIn: (state) => {
      console.log(Date.now() >= state.expToken * 1000)
      return !!state.access_token && !(Date.now() >= state.expToken * 1000)
    }
  },
  actions: {
    login: ({commit}, authData) => {
      return auth.login(authData, (res) => {
        let token = res.data.access_token
        let tokenJson = jwtDecode(token)
        if (tokenJson.authorities.includes('ROLE_ADMIN')) {
          commit('login', {
            access_token: res.data.access_token,
            refresh_token: res.data.refresh_token
          })
          router.push('/')
          return {status: 'OK'}
        } else {
          return {status: 'NOK', message: 'User is not admin'}
        }
      })
    },
    logout: ({commit}) => {
      commit('logout')
      router.push('/login')
    }
  }
}

export default moduleAuth
