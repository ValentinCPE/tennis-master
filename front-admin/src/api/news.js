/**
 * Mocking client-server processing
 */
import axios from '../services/axios-news'

export default {
  getNews (cb) {
    axios
      .get('/news')
      .then((res) => {
        cb(res.data)
      })
  },
  getOneNews (id, cb) {
    axios
      .get('/news/' + id)
      .then((res) => {
        if (res.data.classement === null) {
          res.data.classement = {
            idClassement: null
          }
        }
        cb(res.data)
      })
  },
  deleteNews (id, cb) {
    axios
      .delete('/news/' + id)
      .then((res) => {
        cb(res.data)
      })
  },
  addNews (data, cb) {
    axios
      .post('/news', data)
      .then((res) => {
        cb(res.data)
      })
  },
  updateNews (data, cb) {
    axios
      .put('/news/' + data.idNews, data)
      .then((res) => {
        cb(res.data)
      })
  }
}
