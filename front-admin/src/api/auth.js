import axios from '@/services/axios-auth'

export default {
  login (authData, cb) {
    return new Promise((resolve, reject) => {
      axios.post('/oauth/token', {}, {
        params: {
          grant_type: 'password',
          username: authData.username,
          password: authData.password
        }
      })
        .then(res => {
          let checkResult = cb(res)
          console.log(checkResult)
          if (checkResult.status === 'NOK') {
            // eslint-disable-next-line
            reject({
              status: 401,
              data: {error_description: checkResult.message}
            })
          }
          resolve(res)
        })
        .catch(error => reject(error.response))
    })
  },
  resetPassword (data, cb) {
    axios.post('/public/user/setNewPasswordWithCode', {}, {
      params: data
    }).then(res => {
      cb(res.data)
    })
  }
}
