/**
 * Mocking client-server processing
 */
import axios from '../services/axios-regles'

export default {
  getRegles (cb) {
    axios
      .get('/reglesClub')
      .then((res) => {
        cb(res.data)
      })
  },
  updateReglesClub (data, cb) {
    axios
      .put('/reglesClub', data)
      .then((res) => {
        cb(res.data)
      })
  }
}
