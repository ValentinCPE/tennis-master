package org.cpe.tennismaster.user.common.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotNull;
import java.util.Set;

public class ClassementDto {

    private Long idClassement;

    @NotNull
    private String libelleClassement;

    public ClassementDto() {

    }

    public ClassementDto(String libelleClassement) {
        this.libelleClassement = libelleClassement;
    }

    public Long getIdClassement() {
        return idClassement;
    }

    public void setIdClassement(Long idClassement) {
        this.idClassement = idClassement;
    }

    public String getLibelleClassement() {
        return libelleClassement;
    }

    public void setLibelleClassement(String libelleClassement) {
        this.libelleClassement = libelleClassement;
    }
}
