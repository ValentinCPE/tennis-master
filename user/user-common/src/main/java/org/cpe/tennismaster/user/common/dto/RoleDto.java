package org.cpe.tennismaster.user.common.dto;


import lombok.Builder;

import javax.validation.constraints.NotNull;

public class RoleDto {

    private Integer idRole;

    @NotNull
    private String libelleRole;

    @NotNull
    private String libelleRoleAuth;

    public RoleDto() {
    }

    public RoleDto(String libelleRole, String libelleRoleAuth) {
        this.libelleRole = libelleRole;
        this.libelleRoleAuth = libelleRoleAuth;
    }

    public Integer getIdRole() {
        return idRole;
    }

    public void setIdRole(Integer idRole) {
        this.idRole = idRole;
    }

    public String getLibelleRole() {
        return libelleRole;
    }

    public void setLibelleRole(String libelleRole) {
        this.libelleRole = libelleRole;
    }

    public String getLibelleRoleAuth() {
        return libelleRoleAuth;
    }

    public void setLibelleRoleAuth(String libelleRoleAuth) {
        this.libelleRoleAuth = libelleRoleAuth;
    }
}
