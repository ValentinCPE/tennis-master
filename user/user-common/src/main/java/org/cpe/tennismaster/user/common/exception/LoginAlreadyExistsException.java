package org.cpe.tennismaster.user.common.exception;

import org.cpe.tennismaster.common.exception.GeneralException;
import org.springframework.http.HttpStatus;

public class LoginAlreadyExistsException extends GeneralException {

    public LoginAlreadyExistsException(String login){
        super(login);
    }

    @Override
    protected HttpStatus defineStatusHttp() {
        return HttpStatus.CONFLICT;
    }

    @Override
    protected String defineErrorMessage(String variable) {
        return "Le login " + variable + " existe déjà.";
    }

}
