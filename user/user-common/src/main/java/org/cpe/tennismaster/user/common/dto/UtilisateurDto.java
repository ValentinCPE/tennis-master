package org.cpe.tennismaster.user.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import org.cpe.tennismaster.common.model.utilisateur.Gender;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Builder
@AllArgsConstructor
public class UtilisateurDto {

    @NotNull
    private String nomUtilisateur;

    @NotNull
    private String prenomUtilisateur;

    @NotNull
    private String emailUtilisateur;

    @NotNull
    private String loginUtilisateur;

    @NotNull
    private Date dateNaissanceUtilisateur;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateInscriptionUtilisateur;

    private Boolean estActiveUtilisateur;

    @NotNull
    private Gender genreUtilisateur;

    @NotNull
    private String motPasseUtilisateur;

    @NotNull
    private String adresse1Utilisateur;

    private String adresse2Utilisateur;

    @NotNull
    private String cpUtilisateur;

    @NotNull
    private String villeUtilisateur;

    private Boolean presenceAnnuaire;

    private RoleDto role;

    private ClassementDto classement;

    public UtilisateurDto() {
    }

    public UtilisateurDto(String nomUtilisateur, String prenomUtilisateur, String emailUtilisateur, Gender genreUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
        this.prenomUtilisateur = prenomUtilisateur;
        this.emailUtilisateur = emailUtilisateur;
        this.genreUtilisateur = genreUtilisateur;
        this.dateInscriptionUtilisateur = new Date();
    }

    public UtilisateurDto(String nomUtilisateur, String prenomUtilisateur, String emailUtilisateur, Date dateInscriptionUtilisateur, Gender genreUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
        this.prenomUtilisateur = prenomUtilisateur;
        this.emailUtilisateur = emailUtilisateur;
        this.dateInscriptionUtilisateur = dateInscriptionUtilisateur;
        this.genreUtilisateur = genreUtilisateur;
    }

    public String getNomUtilisateur() {
        return nomUtilisateur;
    }

    public void setNomUtilisateur(String nomUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
    }

    public String getPrenomUtilisateur() {
        return prenomUtilisateur;
    }

    public void setPrenomUtilisateur(String prenomUtilisateur) {
        this.prenomUtilisateur = prenomUtilisateur;
    }

    public String getEmailUtilisateur() {
        return emailUtilisateur;
    }

    public void setEmailUtilisateur(String emailUtilisateur) {
        this.emailUtilisateur = emailUtilisateur;
    }

    public Date getDateInscriptionUtilisateur() {
        return dateInscriptionUtilisateur;
    }

    public void setDateInscriptionUtilisateur(Date dateInscriptionUtilisateur) {
        this.dateInscriptionUtilisateur = dateInscriptionUtilisateur;
    }

    public Boolean getEstActiveUtilisateur() {
        return estActiveUtilisateur;
    }

    public void setEstActiveUtilisateur(Boolean estActiveUtilisateur) {
        this.estActiveUtilisateur = estActiveUtilisateur;
    }

    public Gender getGenreUtilisateur() {
        return genreUtilisateur;
    }

    public void setGenreUtilisateur(Gender genreUtilisateur) {
        this.genreUtilisateur = genreUtilisateur;
    }

    public String getMotPasseUtilisateur() {
        return motPasseUtilisateur;
    }

    public void setMotPasseUtilisateur(String motPasseUtilisateur) {
        this.motPasseUtilisateur = motPasseUtilisateur;
    }

    public RoleDto getRole() {
        return role;
    }

    public void setRole(RoleDto role) {
        this.role = role;
    }

    public Date getDateNaissanceUtilisateur() {
        return dateNaissanceUtilisateur;
    }

    public void setDateNaissanceUtilisateur(Date dateNaissanceUtilisateur) {
        this.dateNaissanceUtilisateur = dateNaissanceUtilisateur;
    }

    public String getLoginUtilisateur() {
        return loginUtilisateur;
    }

    public void setLoginUtilisateur(String loginUtilisateur) {
        this.loginUtilisateur = loginUtilisateur;
    }

    public String getAdresse1Utilisateur() {
        return adresse1Utilisateur;
    }

    public void setAdresse1Utilisateur(String adresse1Utilisateur) {
        this.adresse1Utilisateur = adresse1Utilisateur;
    }

    public String getAdresse2Utilisateur() {
        return adresse2Utilisateur;
    }

    public void setAdresse2Utilisateur(String adresse2Utilisateur) {
        this.adresse2Utilisateur = adresse2Utilisateur;
    }

    public String getCpUtilisateur() {
        return cpUtilisateur;
    }

    public void setCpUtilisateur(String cpUtilisateur) {
        this.cpUtilisateur = cpUtilisateur;
    }

    public String getVilleUtilisateur() {
        return villeUtilisateur;
    }

    public void setVilleUtilisateur(String villeUtilisateur) {
        this.villeUtilisateur = villeUtilisateur;
    }

    public ClassementDto getClassement() {
        return classement;
    }

    public void setClassement(ClassementDto classement) {
        this.classement = classement;
    }

    public Boolean getPresenceAnnuaire() {
        return presenceAnnuaire;
    }

    public void setPresenceAnnuaire(Boolean presenceAnnuaire) {
        this.presenceAnnuaire = presenceAnnuaire;
    }
}
