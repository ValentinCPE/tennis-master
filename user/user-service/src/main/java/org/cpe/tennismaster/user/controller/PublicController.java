package org.cpe.tennismaster.user.controller;

import exception.ActivationException;
import org.cpe.tennismaster.common.model.utilisateur.UtilisateurVue;
import org.cpe.tennismaster.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping(value = "public")
public class PublicController {

    private static final Logger logger = LoggerFactory.getLogger(PublicController.class);

    private final UserService userService;

    @Autowired
    public PublicController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "user/activate", produces = { "application/json" })
    public void activate(@RequestParam(value = "email") String email,
                         @RequestParam(value = "activationCode") String activationCode,
                         @RequestParam(value = "redirectUrl") String redirectUrl,
                         HttpServletResponse response) throws ActivationException, IOException {
        userService.activate(email, activationCode);
        response.sendRedirect(redirectUrl);
    }

    @PostMapping(value = "user/reinitPassword", produces = { "application/json" })
    public ResponseEntity<Boolean> reinitPassword(@RequestParam(value = "email") String email,
                                                  HttpServletRequest request) {
        String host = request.getServerName();
        return ResponseEntity.ok(userService.reinitPassword(email, host));
    }

    @PostMapping(value = "user/setNewPasswordWithCode", produces = { "application/json" })
    public ResponseEntity<UtilisateurVue> setNewPasswordWithCode(@RequestParam(value = "email") String email,
                                                                 @RequestParam(value = "code") String code,
                                                                 @RequestParam(value = "newPassword") String newPassword) {
        return ResponseEntity.ok(userService.setNewPasswordWithCode(email, code, newPassword));
    }

}