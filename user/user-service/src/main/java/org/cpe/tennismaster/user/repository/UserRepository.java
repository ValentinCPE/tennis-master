package org.cpe.tennismaster.user.repository;

import org.cpe.tennismaster.user.model.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Utilisateur, Integer> {

    List<Utilisateur> findAllBy();

    Optional<Utilisateur> findByLoginUtilisateur(String login);

    Optional<Utilisateur> findByEmailUtilisateur(String email);

    Optional<List<Utilisateur>> findByPresenceAnnuaire(boolean presenceAnnuaire);
}
