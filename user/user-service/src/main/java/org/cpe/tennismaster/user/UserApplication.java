package org.cpe.tennismaster.user;

import org.cpe.tennismaster.common.CommonGeneralApplication;
import org.cpe.tennismaster.user.service.UserService;
import org.cpe.tennismaster.user.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import javax.annotation.PostConstruct;

@Import(CommonGeneralApplication.class)
@EnableResourceServer
@SpringBootApplication
public class UserApplication {

    @Autowired
    UserServiceImpl userServiceImpl;

    @PostConstruct
    private void init() {
        userServiceImpl.createAdminAtStart();
    }

    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }

}
