package org.cpe.tennismaster.user.controller;

import lombok.extern.slf4j.Slf4j;
import org.cpe.tennismaster.user.common.dto.ClassementDto;
import org.cpe.tennismaster.common.model.utilisateur.ClassementVue;
import org.cpe.tennismaster.user.service.RankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/rankings")
public class RankController {

    @Autowired
    RankService rankService;

    @GetMapping
    public ResponseEntity<List<ClassementVue>> getAllRank() {
        return ResponseEntity.ok(rankService.getAllRank());
    }

    @GetMapping("{id}")
    public ResponseEntity<ClassementVue> getRankById(@PathVariable Integer id) {
        return ResponseEntity.ok(rankService.getRankById(id));
    }

    @GetMapping("label/{rankingLabel}")
    public ResponseEntity<ClassementVue> getRankByLabel(@PathVariable String rankingLabel) {
        return ResponseEntity.ok(rankService.getRankByLabel(rankingLabel));
    }

    @PostMapping
    public ResponseEntity<ClassementVue> createRank(@RequestBody ClassementDto classement) {
        return ResponseEntity.status(HttpStatus.CREATED).body(rankService.createRank(classement));
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleteRank(@PathVariable Integer id) {
        return ResponseEntity.ok(rankService.deleteRank(id));
    }

    @PutMapping("{id}")
    public ResponseEntity<ClassementVue> updateRank(@PathVariable Integer id, @RequestBody ClassementDto classement) {
        return ResponseEntity.ok(rankService.updateRank(id, classement));
    }
}
