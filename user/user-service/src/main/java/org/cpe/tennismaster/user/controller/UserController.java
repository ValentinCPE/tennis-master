package org.cpe.tennismaster.user.controller;

import lombok.extern.slf4j.Slf4j;

import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.roles.Roles;
import org.cpe.tennismaster.user.common.dto.UtilisateurDto;
import org.cpe.tennismaster.common.model.utilisateur.UtilisateurVue;
import org.cpe.tennismaster.user.common.vue.UtilisateurAnnuaireVue;
import org.cpe.tennismaster.user.exception.AjoutTokenUtilisateurException;
import org.cpe.tennismaster.user.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<List<UtilisateurVue>> getAllUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @GetMapping("names")
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<String> getAllUsersNames() {
        return ResponseEntity.ok(userService.getAllUsersNames());
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('"+ Roles.Libelle.USER + "','"+Roles.Libelle.ADMIN + "')")
    public ResponseEntity<UtilisateurVue> getUserById(@PathVariable Integer id) {
        return ResponseEntity.ok(userService.getUserById(id));
    }

    @GetMapping("email/{email}")
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<UtilisateurVue> getUserByEmail(@PathVariable String email) {
        return ResponseEntity.ok(userService.getUserByEmail(email));
    }

    @PostMapping
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<UtilisateurVue> createUser(@RequestBody UtilisateurDto utilisateurDto, HttpServletRequest request) {
        String host = request.getServerName();
        int port = request.getServerPort();
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.createUser(utilisateurDto, host, port));
    }

    @PostMapping(value = "/signup")
    public ResponseEntity<UtilisateurVue> signUp(@RequestBody UtilisateurDto utilisateurDto, HttpServletRequest request) {
        String host = request.getServerName();
        int port = request.getServerPort();
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.signUp(utilisateurDto, host, port));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity deleteUser(@PathVariable Integer id) {
        return ResponseEntity.ok(userService.deleteUser(id));
    }

    @PutMapping("updateUser/{id}")
    @PreAuthorize("hasAnyRole('"+ Roles.Libelle.USER + "','"+Roles.Libelle.ADMIN + "')")
    public ResponseEntity<UtilisateurVue> updateUser(@PathVariable Integer id, @RequestBody UtilisateurDto utilisateurDto) {
        return ResponseEntity.ok(userService.updateUser(id, utilisateurDto));
    }

    @GetMapping(value = "/annuaire")
    @PreAuthorize("hasAnyRole('"+Roles.Libelle.USER + "','"+Roles.Libelle.ADMIN + "')")
    public ResponseEntity<List<UtilisateurAnnuaireVue>> getAnnuaireJoueur() {
        return ResponseEntity.ok(userService.getAnnuaireJoueur());
    }

    @GetMapping(value = "/me")
    @PreAuthorize("hasAnyRole('"+Roles.Libelle.USER + "','"+Roles.Libelle.ADMIN + "')")
    public ResponseEntity<UtilisateurVue> me(final Principal principal) throws NotFoundException {
        UtilisateurVue user = userService.getUser(principal);
        return ResponseEntity.ok(user);
    }

    @PostMapping(value = "/setTokenPhone")
    @PreAuthorize("hasAnyRole('"+Roles.Libelle.USER + "','"+Roles.Libelle.ADMIN + "')")
    public ResponseEntity<UtilisateurVue> setTokenPhone(@RequestParam(value = "token") String token, final Principal principal) throws NotFoundException, AjoutTokenUtilisateurException {
        UtilisateurVue user = userService.setTokenPhone(token, principal);
        return ResponseEntity.ok(user);
    }
}
