package org.cpe.tennismaster.user.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.common.utils.UtilsTools;
import org.cpe.tennismaster.user.common.dto.RoleDto;
import org.cpe.tennismaster.common.model.utilisateur.RoleVue;
import org.cpe.tennismaster.user.model.Role;
import org.cpe.tennismaster.user.repository.RoleRepository;
import org.cpe.tennismaster.user.service.RoleService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    private ModelMapper modelMapper;


    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public List<RoleVue> getAllRole() {
        List<Role> roleList = this.roleRepository.findAllBy();
        Type listType = new TypeToken<List<RoleVue>>() {}.getType();
        return modelMapper.map(roleList, listType);
    }

    public RoleVue getRoleById(Integer id) throws ParameterException {
        Optional<Role> optRole = roleRepository.findById(id);
        if (!optRole.isPresent()) {
            throw new ParameterException();
        }
        return modelMapper.map(optRole.get(), RoleVue.class);
    }

    public RoleVue createRole(RoleDto roleDto) throws NotFoundException {
        if (roleDto == null) {
            throw new NotFoundException("Role");
        }

        Role roleModel = modelMapper.map(roleDto, Role.class);
        roleModel = roleRepository.save(roleModel);

        return modelMapper.map(roleModel, RoleVue.class);
    }

    public Void deleteRole(Integer id) throws NotFoundException {
        Optional<Role> optRole = roleRepository.findById(id);
        if (!optRole.isPresent()) {
            throw new NotFoundException("Role");
        }
        roleRepository.delete(optRole.get());
        return null;
    }

    public RoleVue updateRole(Integer id, RoleDto roleDto) throws NotFoundException {
        if (roleDto == null) {
            throw new NotFoundException("Role");
        }
        Optional<Role> optExistingUser = roleRepository.findById(id);
        if (!optExistingUser.isPresent()) {
            throw new NotFoundException("Role");
        }
        Role utilisateurModeleTemp = modelMapper.map(roleDto, Role.class);
        UtilsTools.copyNonNullProperties(utilisateurModeleTemp, optExistingUser.get());
        Role utilisateurUpdated = roleRepository.save(optExistingUser.get());
        if (utilisateurUpdated == null) {
            throw new NotFoundException("Role");
        }
        return modelMapper.map(utilisateurUpdated, RoleVue.class);
    }
}
