package org.cpe.tennismaster.user.controller;


import lombok.extern.slf4j.Slf4j;
import org.cpe.tennismaster.common.roles.Roles;
import org.cpe.tennismaster.user.common.dto.RoleDto;
import org.cpe.tennismaster.common.model.utilisateur.RoleVue;
import org.cpe.tennismaster.user.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


//@RolesAllowed({ADMIN})
@Slf4j
@RestController
@RequestMapping("/roles")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping
    public ResponseEntity<List<RoleVue>> getAllRole() {
        return ResponseEntity.ok(roleService.getAllRole());
    }

    @GetMapping("{id}")
    public ResponseEntity<RoleVue> getRoleById(@PathVariable Integer id) {
        return ResponseEntity.ok(roleService.getRoleById(id));
    }

    @PostMapping
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<RoleVue> createRole(@RequestBody RoleDto roleDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(roleService.createRole(roleDto));
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity deleteRole(@PathVariable Integer id) {
        return ResponseEntity.ok(roleService.deleteRole(id));
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<RoleVue> updateRole(@PathVariable Integer id, @RequestBody RoleDto roleDto) {
        return ResponseEntity.ok(roleService.updateRole(id, roleDto));
    }
}
