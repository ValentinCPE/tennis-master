package org.cpe.tennismaster.user.repository;

import org.cpe.tennismaster.user.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    List<Role> findAllBy();

    Role findByLibelleRole(String libelleRole);

    Role findByLibelleRoleAuth(String libelleRoleAuth);

}
