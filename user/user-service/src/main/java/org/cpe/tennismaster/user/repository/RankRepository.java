package org.cpe.tennismaster.user.repository;

import org.cpe.tennismaster.user.model.Classement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RankRepository extends JpaRepository<Classement, Integer> {

    List<Classement> findAllBy();
    Optional<Classement> findByLibelleClassement(String libelleClassement);
}
