package org.cpe.tennismaster.user.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.common.utils.UtilsTools;
import org.cpe.tennismaster.user.common.dto.ClassementDto;
import org.cpe.tennismaster.common.model.utilisateur.ClassementVue;
import org.cpe.tennismaster.user.model.Classement;
import org.cpe.tennismaster.user.repository.RankRepository;
import org.cpe.tennismaster.user.service.RankService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class RankServiceImpl implements RankService {

    private final RankRepository rankRepository;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    public RankServiceImpl(RankRepository rankRepository) {
        this.rankRepository = rankRepository;
    }

    public List<ClassementVue> getAllRank() {
        List<Classement> classementList = this.rankRepository.findAllBy();
        Type listType = new TypeToken<List<ClassementVue>>() {}.getType();
        return modelMapper.map(classementList, listType);
    }

    public ClassementVue getRankById(@PathVariable Integer id) throws ParameterException {
        Optional<Classement> optRank = rankRepository.findById(id);
        if (!optRank.isPresent()) {
            throw new ParameterException();
        }
        return modelMapper.map(optRank.get(), ClassementVue.class);
    }

    public ClassementVue getRankByLabel(@PathVariable String rankingLabel) throws ParameterException {
        Optional<Classement> optRank = rankRepository.findByLibelleClassement(rankingLabel);
        if (!optRank.isPresent()) {
            throw new ParameterException();
        }
        return modelMapper.map(optRank.get(), ClassementVue.class);
    }

    @Override
    public ClassementVue createRank(ClassementDto classementDto) throws NotFoundException {
        if (classementDto == null) {
            throw new NotFoundException("Classement");
        }

        Classement classementModel = modelMapper.map(classementDto, Classement.class);
        classementModel = rankRepository.save(classementModel);

        return modelMapper.map(classementModel, ClassementVue.class);
    }

    @Override
    public Void deleteRank(Integer id) throws NotFoundException {
        Optional<Classement> optClassement = rankRepository.findById(id);
        if (!optClassement.isPresent()) {
            throw new NotFoundException("Classement");
        }
        rankRepository.delete(optClassement.get());
        return null;
    }

    @Override
    public ClassementVue updateRank(Integer id, ClassementDto classementDto) throws NotFoundException {
        if (classementDto == null) {
            throw new NotFoundException("Classement");
        }
        Optional<Classement> optExistingClassement = rankRepository.findById(id);
        if (!optExistingClassement.isPresent()) {
            throw new NotFoundException("Classement");
        }
        Classement classementModeleTemp = modelMapper.map(classementDto, Classement.class);
        UtilsTools.copyNonNullProperties(classementModeleTemp, optExistingClassement.get());
        Classement classementUpdated = rankRepository.save(optExistingClassement.get());
        if (classementUpdated == null) {
            throw new NotFoundException("Classement");
        }
        return modelMapper.map(classementUpdated, ClassementVue.class);
    }
}
