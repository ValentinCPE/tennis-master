package org.cpe.tennismaster.user.model;

import org.cpe.tennismaster.common.model.utilisateur.Gender;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class GenderConverter implements AttributeConverter<Gender, Character> {
    @Override
    public Character convertToDatabaseColumn(Gender gender) {
        return gender.getName();
    }

    @Override
    public Gender convertToEntityAttribute(Character dbData) {
        return Gender.getByName(dbData);
    }
}
