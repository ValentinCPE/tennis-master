package org.cpe.tennismaster.user.service.impl;

import exception.ActivationException;
import lombok.extern.slf4j.Slf4j;

import model.UserAuthorizationDto;
import org.apache.commons.lang3.RandomStringUtils;
import org.cpe.tennismaster.common.exception.*;
import org.cpe.tennismaster.common.model.Intent;
import org.cpe.tennismaster.common.model.Letter;
import org.cpe.tennismaster.common.model.LienService;
import org.cpe.tennismaster.common.model.ResponseView;
import org.cpe.tennismaster.common.model.utilisateur.Gender;
import org.cpe.tennismaster.common.roles.Roles;
import org.cpe.tennismaster.common.utils.UtilsTools;
import org.cpe.tennismaster.reservation.common.vue.ReservationVue;
import org.cpe.tennismaster.user.common.dto.UtilisateurDto;
import org.cpe.tennismaster.user.common.exception.EmailAlreadyExistsException;
import org.cpe.tennismaster.user.common.exception.LoginAlreadyExistsException;
import org.cpe.tennismaster.common.model.utilisateur.UtilisateurVue;
import org.cpe.tennismaster.user.common.vue.UtilisateurAnnuaireVue;
import org.cpe.tennismaster.notification.common.model.IntentNotification;
import org.cpe.tennismaster.notification.common.model.MailDto;
import org.cpe.tennismaster.notification.common.model.NotificationDto;
import org.cpe.tennismaster.notification.common.model.NotificationType;
import org.cpe.tennismaster.user.exception.*;
import org.cpe.tennismaster.user.model.ActivationRedis;
import org.cpe.tennismaster.user.model.Role;
import org.cpe.tennismaster.user.model.Utilisateur;
import org.cpe.tennismaster.user.repository.ActivationRedisRepository;
import org.cpe.tennismaster.user.repository.RoleRepository;
import org.cpe.tennismaster.user.repository.UserRepository;
import org.cpe.tennismaster.user.service.UserActiveMQOperation;
import org.cpe.tennismaster.user.service.UserService;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.security.Principal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.*;
import java.util.regex.Pattern;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    private static final String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
            "[a-zA-Z0-9_+&*-]+)*@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
            "A-Z]{2,7}$";

    private static final String passwordRegex = "(?=.*[A-Z])" +  //At least one upper case character (A-Z)
            "(?=.*[a-z])" +     //At least one lower case character (a-z)
            "(?=.*\\d)" +   //At least one digit (0-9)
            "(?=.*\\p{Punct})" +  //At least one special character (Punctuation)
            ".*";

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final ActivationRedisRepository activationRedisRepository;

    private final UserActiveMQOperation userActiveMQOperation;

    private final ModelMapper modelMapper;

    private final UtilsTools utilsTools;

    @Value(value = "${authorization.baseUrl}")
    private String baseUrlAuthorization;

    @Value(value = "${authorization.signup}")
    private String signupUrl;

    @Value(value = "${authorization.activate}")
    private String activateUrl;

    @Value(value = "${authorization.generatePassword}")
    private String generatePasswordUrl;

    @Value(value = "http://${service-reservation.ip}"+":${service-reservation.port}"+"/${service-reservation.get}/")
    private String baseUrlReservation;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, ActivationRedisRepository activationRedisRepository, UserActiveMQOperation userActiveMQOperation, ModelMapper modelMapper, UtilsTools utilsTools) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.activationRedisRepository = activationRedisRepository;
        this.userActiveMQOperation = userActiveMQOperation;
        this.modelMapper = modelMapper;
        this.utilsTools = utilsTools;
    }

    public List<UtilisateurVue> getAllUsers() {
        List<Utilisateur> utilisateurList =  this.userRepository.findAllBy();
        Type listType = new TypeToken<List<UtilisateurVue>>() {}.getType();
        return modelMapper.map(utilisateurList, listType);
    }

    public String getAllUsersNames() {
        List<Utilisateur> utilisateurList = this.userRepository.findAllBy();
        Type listType = new TypeToken<List<UtilisateurVue>>() {}.getType();
        List<UtilisateurVue> utilisateurVueList =  modelMapper.map(utilisateurList, listType);
        StringBuilder allUsersNameJson = new StringBuilder();
        allUsersNameJson.append('[');

        utilisateurVueList.forEach(user -> {
            allUsersNameJson.append(userFilter(user));
        });
        //Suppression de la dernière virgule
        allUsersNameJson.deleteCharAt(allUsersNameJson.length() - 1);
        allUsersNameJson.append(']');
        return allUsersNameJson.toString();
    }

    private String userFilter(UtilisateurVue utilisateur) {
        return "{\"id\":" + utilisateur.getIdUtilisateur() + ",\"firstName\":\"" + utilisateur.getPrenomUtilisateur() + "\",\"lastName\":\"" + utilisateur.getNomUtilisateur() + "\"},";
    }

    public UtilisateurVue getUserById(Integer id) throws NotFoundException {
        Optional<Utilisateur> optUser = userRepository.findById(id);
        if (!optUser.isPresent()) {
            throw new NotFoundException("Utilisateur");
        }
        return modelMapper.map(optUser.get(), UtilisateurVue.class);
    }

    public Utilisateur getUserByLogin(String login) throws NotFoundException {
        Optional<Utilisateur> utilisateur = userRepository.findByLoginUtilisateur(login);
        if (!utilisateur.isPresent()) {
            throw new NotFoundException("Utilisateur");
        }
        return utilisateur.get();
    }

    public UtilisateurVue getUserByEmail(String email) throws NotFoundException {
        Optional<Utilisateur> utilisateur = userRepository.findByEmailUtilisateur(email);
        if (!utilisateur.isPresent()) {
            throw new NotFoundException("Utilisateur");
        }
        return modelMapper.map(utilisateur.get(), UtilisateurVue.class);
    }

    public Void deleteUser(Integer id) throws NotFoundException {
        Optional<Utilisateur> optUser = userRepository.findById(id);
        if (!optUser.isPresent()) {
            throw new NotFoundException("User");
        }
        Map<String, String> params = new HashMap<>();
        params.put("userId", id.toString());
        List<ReservationVue> reservationVueList = utilsTools.restCall(baseUrlReservation+"/byUserId", params, HttpMethod.GET, new ParameterizedTypeReference<List<ReservationVue>>() {});
        if(!reservationVueList.isEmpty()) {
            for(ReservationVue reservationVue : reservationVueList) { //TODO: a changer pour faire un appel avec tous les id
                utilsTools.restCall(baseUrlReservation+reservationVue.getIdReservation(), HttpMethod.DELETE, new ParameterizedTypeReference<Object>() {});
            }
        }
        userRepository.delete(optUser.get());
        return null;
    }

    public UtilisateurVue updateUser(Integer id, UtilisateurDto utilisateurDto) throws NotFoundException {
        if (utilisateurDto == null) {
            throw new NotFoundException("User");
        }
        Optional<Utilisateur> optExistingUser = userRepository.findById(id);
        if (!optExistingUser.isPresent()) {
            throw new NotFoundException("User");
        }
        Utilisateur utilisateurModeleTemp = modelMapper.map(utilisateurDto, Utilisateur.class);
        if(utilisateurModeleTemp.getMotPasseUtilisateur() != null) {
            if(!passwordIsValid(utilisateurModeleTemp.getMotPasseUtilisateur())){
                throw new PasswordNotCorrectException();
            }

            // encode password
            utilisateurModeleTemp.setMotPasseUtilisateur(this.getEncodedPasswordString(utilisateurModeleTemp.getMotPasseUtilisateur()));
        }
        UtilsTools.copyNonNullProperties(utilisateurModeleTemp, optExistingUser.get());
        Utilisateur utilisateurUpdated = userRepository.save(optExistingUser.get());

        return modelMapper.map(utilisateurUpdated, UtilisateurVue.class);
    }

    private Utilisateur userCreationDB(UtilisateurDto utilisateurDto) throws EmailAlreadyExistsException, CreationUtilisateurException, InternalServerException {
        if(userRepository.findByLoginUtilisateur(utilisateurDto.getLoginUtilisateur()).isPresent()){
            throw new LoginAlreadyExistsException(utilisateurDto.getLoginUtilisateur());
        }

        if(userRepository.findByEmailUtilisateur(utilisateurDto.getEmailUtilisateur()).isPresent()){
            throw new EmailAlreadyExistsException(utilisateurDto.getEmailUtilisateur());
        }

        Utilisateur utilisateurModel = modelMapper.map(utilisateurDto, Utilisateur.class);

        // define role user
        Role roleUser = roleRepository.findByLibelleRoleAuth("ROLE_USER");
        utilisateurModel.setRole(roleUser);

        if(!passwordIsValid(utilisateurModel.getMotPasseUtilisateur())){
            throw new PasswordNotCorrectException();
        }

        // encode password
        utilisateurModel.setMotPasseUtilisateur(this.getEncodedPasswordString(utilisateurDto.getMotPasseUtilisateur()));

        return userRepository.save(utilisateurModel);
    }

    @Override
    public UtilisateurVue createUser(UtilisateurDto utilisateurDto, String host, int port) throws EmailAlreadyExistsException, CreationUtilisateurException, InternalServerException {

        Utilisateur utilisateur = userCreationDB(utilisateurDto);

        utilisateur.setEstActiveUtilisateur(true);
        userRepository.save(utilisateur);

        logger.info("New user {} has been created and is activated", utilisateur.getEmailUtilisateur());

        return modelMapper.map(utilisateur, UtilisateurVue.class);
    }

    @Override
    public UtilisateurVue signUp(UtilisateurDto utilisateurDto, String host, int port) throws EmailAlreadyExistsException, CreationUtilisateurException, InternalServerException {

        Utilisateur utilisateur = userCreationDB(utilisateurDto);

        ActivationRedis activationRedis = ActivationRedis.builder()
                .idUser(utilisateur.getIdUtilisateur())
                .activationId(RandomStringUtils.random(12, true, true))
                .build();

        activationRedisRepository.save(activationRedis);

        // Envoi mail activation
        this.sendMailActivation(utilisateur, activationRedis.getActivationId(), host, port);

        logger.info("New user {} has been created with activation code : {}", utilisateur.getEmailUtilisateur(), activationRedis.getActivationId());

        return modelMapper.map(utilisateur, UtilisateurVue.class);
    }

    @Override
    public UtilisateurVue getUser(Principal principal) throws NotFoundException {
        Optional<Utilisateur> user = userRepository.findByLoginUtilisateur(principal.getName());
        if(!user.isPresent()){
            throw new NotFoundException("Utilisateur " + principal.getName());
        }
        return modelMapper.map(user.get(), UtilisateurVue.class);
    }

    @Override
    public UtilisateurVue updateLastConnectionDate(String uuidOrEmail, String timestamp) {
        Optional<Utilisateur> user;

        if(uuidOrEmail == null || uuidOrEmail.isEmpty()) {
            logger.error("Uuid or email null during the update of last connection date");
            return null;
        }

        if(!uuidOrEmail.contains("@")){
            Integer id = Integer.valueOf(uuidOrEmail);
            user = userRepository.findById(id);
        } else {
            user = userRepository.findByEmailUtilisateur(uuidOrEmail);
        }

        if(!user.isPresent()) {
            logger.error("User {} not found, last connection date cannot be updated !", uuidOrEmail);
            return null;
        }

        Utilisateur userUpdated = user.get();
        userUpdated.setDateDerniereConnexionUtilisateur(new Timestamp(Long.parseLong(timestamp)));
        userUpdated = userRepository.save(userUpdated);
        logger.info("Last connection date for User {} has been updated : {}", userUpdated.getEmailUtilisateur(), userUpdated.getDateDerniereConnexionUtilisateur().toString());
        return modelMapper.map(userUpdated, UtilisateurVue.class);
    }

    @Override
    public UtilisateurVue activate(String email, String activationCode) throws ActivationException {
        if(activationCode.isEmpty()) throw new ActivationException();
        Optional<Utilisateur> userOptional = userRepository.findByEmailUtilisateur(email);
        if(!userOptional.isPresent()) throw new ActivationException();

        Utilisateur utilisateur = userOptional.get();

        ActivationRedis activationRedis = activationRedisRepository.findByIdUser(utilisateur.getIdUtilisateur());
        if(activationRedis == null || !activationRedis.getActivationId().equals(activationCode)){
            throw new ActivationException();
        }
        utilisateur.setEstActiveUtilisateur(true);
        utilisateur.setDateActivationConnexionUtilisateur(new Timestamp(new Date().getTime()));
        userRepository.save(utilisateur);

        activationRedisRepository.delete(activationRedis);

        logger.info("Utilisateur {} activé.", utilisateur.getEmailUtilisateur());

        return modelMapper.map(utilisateur, UtilisateurVue.class);
    }

    @Override
    public UtilisateurVue setTokenPhone(String token, Principal principal) throws AjoutTokenUtilisateurException {
        Optional<Utilisateur> utilisateur = userRepository.findByLoginUtilisateur(principal.getName());
        if(!utilisateur.isPresent()){
            throw new AjoutTokenUtilisateurException();
        }
        utilisateur.get().setTokenPhone(token);
        userRepository.save(utilisateur.get());
        logger.info("Nouveau token {} pour utilisateur {}", token, utilisateur.get().getEmailUtilisateur());
        return modelMapper.map(utilisateur, UtilisateurVue.class);
    }

    /*@Override
    public boolean reinitPassword(String login, String email) {
        if(login != null && !login.isEmpty()){
            Utilisateur utilisateur = this.getUserByLogin(login);

        }
    } */

    @Override
    public boolean reinitPassword(String email, String host) {
        Utilisateur utilisateur = this.getUtilisateurByEmail(email);
        utilisateur.setMotPasseUtilisateur(UtilsTools.generateString(8));
        userRepository.save(utilisateur);
        this.sendMailReinitPassword(utilisateur, utilisateur.getMotPasseUtilisateur(), host);
        return true;
    }

    @Override
    public UtilisateurVue setNewPasswordWithCode(String email, String code, String newPassword) {
        Utilisateur utilisateur = this.getUtilisateurByEmail(email);
        if(!utilisateur.getMotPasseUtilisateur().equals(code)){
            throw new MauvaisCodeUtilisateurReinitException();
        }
        if(!passwordIsValid(newPassword)){
            throw new PasswordNotCorrectException();
        }
        utilisateur.setMotPasseUtilisateur(this.getEncodedPasswordString(newPassword));
        userRepository.save(utilisateur);
        return modelMapper.map(utilisateur, UtilisateurVue.class);
    }

    @Override
    public List<UtilisateurAnnuaireVue> getAnnuaireJoueur() {
        Optional<List<Utilisateur>> optionalUtilisateurList = userRepository.findByPresenceAnnuaire(true);
        if(optionalUtilisateurList.isPresent()) {
            Type listType = new TypeToken<List<UtilisateurAnnuaireVue>>() {}.getType();
            return modelMapper.map(optionalUtilisateurList.get(), listType);
        }
        return Collections.emptyList();
    }

    private String getEncodedPasswordString(String password) {
        Map<String, String> params = new HashMap<>();
        params.put("password", password);
        return utilsTools.restCall(baseUrlAuthorization + generatePasswordUrl, params, HttpMethod.GET, new ParameterizedTypeReference<String>() {});
    }

    private Utilisateur getUtilisateurByEmail(String email){
        Optional<Utilisateur> utilisateurOpt = userRepository.findByEmailUtilisateur(email);
        if(!utilisateurOpt.isPresent()){
            throw new NotFoundException("Utilisateur");
        }
        return utilisateurOpt.get();
    }

    private void sendMailActivation(Utilisateur utilisateur, String codeActivation, String host, int port){
        MailDto notificationDto = new MailDto();
        notificationDto.setNotificationType(NotificationType.MAIL);
        notificationDto.setAction(IntentNotification.UTILISATEUR_CREE);
        notificationDto.setFrom(null);
        notificationDto.setTo(utilisateur.getEmailUtilisateur());
        notificationDto.setSubject("[Tennis Master] Bienvenue parmi nous !");
        notificationDto.setContent("Inscription réussie, veuillez accepter");

        Map<String,String> imagesAAjouter = new HashMap<>();
        // imagesAAjouter.put("mailboxOpening","mailboxopening.gif");
        // notificationDto.setImagesAAjouter(imagesAAjouter);

        Map<String,Object> model = new HashMap<>();
        model.put("prenom", utilisateur.getPrenomUtilisateur());
        model.put("login", utilisateur.getLoginUtilisateur());
        model.put("urlActivation", "http://" + host + ":" + port + "/public/user/activate?email=" + utilisateur.getEmailUtilisateur() + "&activationCode=" + codeActivation + "&redirectUrl=http://" + host);
        notificationDto.setModel(model);

        ResponseView<NotificationDto> responseView = new ResponseView<>(notificationDto);
        Letter<NotificationDto> letter = new Letter<>(Intent.SEND_MAIL_NOTIFICATION, LienService.NOTIFICATION, responseView);

        userActiveMQOperation.sendMessage(letter);
    }

    private void sendMailReinitPassword(Utilisateur utilisateur, String code, String host){
        MailDto notificationDto = new MailDto();
        notificationDto.setNotificationType(NotificationType.MAIL);
        notificationDto.setAction(IntentNotification.REINIT_PASSWORD);
        notificationDto.setTo(utilisateur.getEmailUtilisateur());
        notificationDto.setSubject("[Tennis Master] Réinitialisation du mot de passe !");
        notificationDto.setContent("Réinitialisation en cours, veuillez continuer");

        Map<String,Object> model = new HashMap<>();
        model.put("code", code);
        model.put("login", utilisateur.getLoginUtilisateur());
        model.put("reinitLink", "http://" + host + "/passwordreset?email=" + utilisateur.getEmailUtilisateur() + "&reinitCode=" + code);
        notificationDto.setModel(model);

        Map<String,String> imagesAAjouter = new HashMap<>();
        // imagesAAjouter.put("forget","willsmithforget.gif");
        // notificationDto.setImagesAAjouter(imagesAAjouter);

        ResponseView<NotificationDto> responseView = new ResponseView<>(notificationDto);
        Letter<NotificationDto> letter = new Letter<>(Intent.SEND_MAIL_NOTIFICATION, LienService.NOTIFICATION, responseView);

        userActiveMQOperation.sendMessage(letter);
    }

    private List<GeneralException> generateErrors(UserAuthorizationDto userAuthorizationDto){
        List<GeneralException> errors = new ArrayList<>();

        if(userAuthorizationDto == null){
            errors.add(new ParameterException());
        }else{
            if(userAuthorizationDto.getEmail().isEmpty() || !mailIsValid(userAuthorizationDto.getEmail())){
                errors.add(new MailNotCorrectException("The format of email you have entered is not correct"));
            }

            // Check condition
        }

        if(errors.size() > 0){
            StringBuilder sb = new StringBuilder();
            for(GeneralException generalException : errors){
                sb.append("- ").append(generalException.getMessage()).append("\n");
            }
            throw new ParametersMultipleErrorsException(sb.toString());
        }

        return errors;
    }

    private static boolean mailIsValid(String email){
        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }

    private static boolean passwordIsValid(String password){
        Pattern pat = Pattern.compile(passwordRegex);
        if (password == null)
            return false;
        return pat.matcher(password).matches() && password.length() >= 8;
    }

    public void createAdminAtStart() {
        UtilisateurDto utilisateurDto = new UtilisateurDto();
        utilisateurDto.setNomUtilisateur("ADMIN");
        utilisateurDto.setPrenomUtilisateur("admin");
        utilisateurDto.setEmailUtilisateur("admin@admin.fr");
        utilisateurDto.setLoginUtilisateur("admin");
        utilisateurDto.setDateNaissanceUtilisateur(new Date());
        utilisateurDto.setGenreUtilisateur(Gender.U);
        utilisateurDto.setMotPasseUtilisateur("@dmiN123");
        utilisateurDto.setAdresse1Utilisateur("admin");
        utilisateurDto.setCpUtilisateur("12345");
        utilisateurDto.setVilleUtilisateur("admin");
        utilisateurDto.setEstActiveUtilisateur(true);

        try {
            Utilisateur utilisateur = userCreationDB(utilisateurDto);
            Role roleUser = roleRepository.findByLibelleRoleAuth(Roles.ADMIN.getLibelle());
            utilisateur.setRole(roleUser);
            userRepository.save(utilisateur);
            logger.info("New user {} has been created and is activated", utilisateur.getEmailUtilisateur());
        } catch (Exception e) {
            logger.warn("Compte admin déjà existant");
        }

    }
}
