package org.cpe.tennismaster.user.exception;

import org.cpe.tennismaster.common.exception.GeneralException;
import org.springframework.http.HttpStatus;

public class AjoutTokenUtilisateurException extends GeneralException {

    public AjoutTokenUtilisateurException() {
        super();
    }

    @Override
    protected HttpStatus defineStatusHttp() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    protected String defineErrorMessage(String variable) {
        return "Impossible d'ajouter le token du téléphone";
    }

}
