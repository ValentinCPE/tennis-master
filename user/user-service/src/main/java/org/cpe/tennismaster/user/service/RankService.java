package org.cpe.tennismaster.user.service;

import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.user.common.dto.ClassementDto;
import org.cpe.tennismaster.common.model.utilisateur.ClassementVue;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public interface RankService {

    List<ClassementVue> getAllRank();

    ClassementVue getRankById(@PathVariable Integer id) throws ParameterException;

    ClassementVue getRankByLabel(@PathVariable String rankingLabel) throws ParameterException;

    ClassementVue createRank(ClassementDto classementDto) throws NotFoundException;

    Void deleteRank(Integer id) throws NotFoundException;

    ClassementVue updateRank(Integer id, ClassementDto classementDto) throws NotFoundException;
}
