package org.cpe.tennismaster.user.service;

import exception.ActivationException;
import org.cpe.tennismaster.common.exception.InternalServerException;
import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.user.common.dto.UtilisateurDto;
import org.cpe.tennismaster.user.common.exception.EmailAlreadyExistsException;
import org.cpe.tennismaster.common.model.utilisateur.UtilisateurVue;
import org.cpe.tennismaster.user.common.vue.UtilisateurAnnuaireVue;
import org.cpe.tennismaster.user.exception.AjoutTokenUtilisateurException;
import org.cpe.tennismaster.user.exception.CreationUtilisateurException;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public interface UserService{

    List<UtilisateurVue> getAllUsers();

    String getAllUsersNames();

    UtilisateurVue getUser(Principal principal) throws NotFoundException;

    UtilisateurVue getUserById(Integer id) throws NotFoundException;

    UtilisateurVue getUserByEmail(String email) throws NotFoundException;

    UtilisateurVue createUser(UtilisateurDto utilisateur, String host, int port) throws EmailAlreadyExistsException, CreationUtilisateurException, InternalServerException;

    UtilisateurVue signUp(UtilisateurDto utilisateur, String host, int port) throws EmailAlreadyExistsException, CreationUtilisateurException, InternalServerException;

    Void deleteUser(Integer id) throws NotFoundException;

    UtilisateurVue updateUser(Integer id, UtilisateurDto utilisateur) throws NotFoundException;

    UtilisateurVue updateLastConnectionDate(String uuidOrEmail, String timestamp);

    UtilisateurVue activate(String email, String activationCode) throws ActivationException;

    UtilisateurVue setTokenPhone(String token, Principal principal) throws AjoutTokenUtilisateurException;

    boolean reinitPassword(String email, String host);

    UtilisateurVue setNewPasswordWithCode(String email, String code, String newPassword);

    List<UtilisateurAnnuaireVue> getAnnuaireJoueur();

    //TODO: --> en fonction de l'utilisateur connecté qui fait la demande
    //ResponseEntity<User> getCurrentUser();
    //ResponseEntity<User> updateUser(User user);
    //ResponseEntity<User> activateUser(@PathVariable Long id);
    //ResponseEntity<User> disableUser(@PathVariable Long id);
}
