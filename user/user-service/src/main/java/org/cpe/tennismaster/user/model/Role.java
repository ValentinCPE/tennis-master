package org.cpe.tennismaster.user.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Builder
@AllArgsConstructor
@Entity
@Table(name = "role")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, name = "id_role")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer idRole;

    @Column(unique = true)
    @NotNull
    private String libelleRole;

    @Column(unique = true)
    @NotNull
    private String libelleRoleAuth;

    public Role() {
    }

    public Role(String libelleRole, String libelleRoleAuth) {
        this.libelleRole = libelleRole;
        this.libelleRoleAuth = libelleRoleAuth;
    }

    public Integer getIdRole() {
        return idRole;
    }

    public void setIdRole(Integer idRole) {
        this.idRole = idRole;
    }

    public String getLibelleRole() {
        return libelleRole;
    }

    public void setLibelleRole(String libelleRole) {
        this.libelleRole = libelleRole;
    }

    public String getLibelleRoleAuth() {
        return libelleRoleAuth;
    }

    public void setLibelleRoleAuth(String libelleRoleAuth) {
        this.libelleRoleAuth = libelleRoleAuth;
    }
}
