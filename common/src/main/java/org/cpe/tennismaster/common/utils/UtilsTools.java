package org.cpe.tennismaster.common.utils;


import org.cpe.tennismaster.common.exception.UnauthorizedException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.*;

@Configuration
public class UtilsTools {

    private final RestTemplate restTemplate;

    @Autowired
    public UtilsTools(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public static void copyNonNullProperties(Object src, Object target) {
        BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
    }

    public static String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<String>();
        for (java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            //field is empty if == null or if it's a float == 0.0f
            if (srcValue == null || (Float.class.isAssignableFrom(srcValue.getClass()) && Math.abs((float) srcValue - 0.0f) < Float.MIN_NORMAL)) {
                emptyNames.add(pd.getName());
            }
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }

    public <T>T restCall(String url, HttpMethod method, ParameterizedTypeReference<T> responseType) {
        return restCall(url, Collections.emptyMap(), method, responseType);
    }

    public <T>T restCall(String url, Map<String, String> params, HttpMethod method, ParameterizedTypeReference<T> responseType) {
        HttpHeaders requestHeaders = new HttpHeaders();
        if(SecurityContextHolder.getContext() != null && !SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser")){
            String token = ((OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails()).getTokenValue();
            requestHeaders.add("Authorization", "Bearer " + token);
        }

        HttpEntity<T> requestEntity = new HttpEntity<>(requestHeaders);

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url);
        params.forEach(uriBuilder::queryParam);

        ResponseEntity<T> responseEntity = restTemplate.exchange(
                uriBuilder.toUriString(),
                method,
                requestEntity,
                responseType
        );

        if (responseEntity.getStatusCode().equals(HttpStatus.UNAUTHORIZED) || responseEntity.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
            throw new UnauthorizedException();
        }

        return responseEntity.getBody();
    }

    public static String generateString(int length) {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();
    }
}
