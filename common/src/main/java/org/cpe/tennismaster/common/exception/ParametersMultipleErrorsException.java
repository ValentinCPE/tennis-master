package org.cpe.tennismaster.common.exception;

import org.springframework.http.HttpStatus;

import java.util.List;

public class ParametersMultipleErrorsException extends GeneralException {

    public ParametersMultipleErrorsException(String variable) {
        super(variable);
    }

    @Override
    protected HttpStatus defineStatusHttp() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    protected String defineErrorMessage(String variable) {
        return "Multiple errors occured :\n" +
                variable;
    }
}
