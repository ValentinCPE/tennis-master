package org.cpe.tennismaster.common.model;

public enum LienService {

    RESERVATION("RESERVATION"), COURT("COURT"), USER("USER"), ALEXA("ALEXA"), NOTIFICATION("NOTIFICATION");

    private String name;

    LienService(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return super.toString();
    }

}

