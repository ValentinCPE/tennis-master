package org.cpe.tennismaster.common.model.utilisateur;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class UtilisateurVue {

    @NotNull
    private Integer idUtilisateur;

    @NotNull
    private String nomUtilisateur;

    @NotNull
    private String prenomUtilisateur;

    @NotNull
    private String emailUtilisateur;

    @NotNull
    private String loginUtilisateur;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateNaissanceUtilisateur;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateDerniereConnexionUtilisateur;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateActivationConnexionUtilisateur;

    private Boolean estActiveUtilisateur;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateInscriptionUtilisateur;

    @NotNull
    private Gender genreUtilisateur;

    @NotNull
    private String adresse1Utilisateur;

    private String adresse2Utilisateur;

    @NotNull
    private String cpUtilisateur;

    @NotNull
    private String villeUtilisateur;

    @NotNull
    private Boolean presenceAnnuaire;

    private RoleVue role;

    private ClassementVue classement;

    public UtilisateurVue() {
    }

    public UtilisateurVue(String nomUtilisateur, String prenomUtilisateur, String emailUtilisateur, Gender genreUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
        this.prenomUtilisateur = prenomUtilisateur;
        this.emailUtilisateur = emailUtilisateur;
        this.genreUtilisateur = genreUtilisateur;
        this.dateInscriptionUtilisateur = new Date();
    }

    public UtilisateurVue(String nomUtilisateur, String prenomUtilisateur, String emailUtilisateur, Date dateInscriptionUtilisateur, Gender genreUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
        this.prenomUtilisateur = prenomUtilisateur;
        this.emailUtilisateur = emailUtilisateur;
        this.dateInscriptionUtilisateur = dateInscriptionUtilisateur;
        this.genreUtilisateur = genreUtilisateur;
    }

    public Integer getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(Integer idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getNomUtilisateur() {
        return nomUtilisateur;
    }

    public void setNomUtilisateur(String nomUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
    }

    public String getPrenomUtilisateur() {
        return prenomUtilisateur;
    }

    public void setPrenomUtilisateur(String prenomUtilisateur) {
        this.prenomUtilisateur = prenomUtilisateur;
    }

    public String getEmailUtilisateur() {
        return emailUtilisateur;
    }

    public void setEmailUtilisateur(String emailUtilisateur) {
        this.emailUtilisateur = emailUtilisateur;
    }

    public Date getDateInscriptionUtilisateur() {
        return dateInscriptionUtilisateur;
    }

    public void setDateInscriptionUtilisateur(Date dateInscriptionUtilisateur) {

        this.dateInscriptionUtilisateur = dateInscriptionUtilisateur;
    }

    public Gender getGenreUtilisateur() {
        return genreUtilisateur;
    }

    public void setGenreUtilisateur(Gender genreUtilisateur) {
        this.genreUtilisateur = genreUtilisateur;
    }

    /*public String getMotPasseUtilisateur() {
        return motPasseUtilisateur;
    }

    public void setMotPasseUtilisateur(String motPasseUtilisateur) {
        this.motPasseUtilisateur = motPasseUtilisateur;
    }*/

    public RoleVue getRole() {
        return role;
    }

    public void setRole(RoleVue role) {
        this.role = role;
    }

    public String getLoginUtilisateur() {
        return loginUtilisateur;
    }

    public void setLoginUtilisateur(String loginUtilisateur) {
        this.loginUtilisateur = loginUtilisateur;
    }

    public Date getDateNaissanceUtilisateur() {
        return dateNaissanceUtilisateur;
    }

    public void setDateNaissanceUtilisateur(Date dateNaissanceUtilisateur) {
        this.dateNaissanceUtilisateur = dateNaissanceUtilisateur;
    }

    public String getAdresse1Utilisateur() {
        return adresse1Utilisateur;
    }

    public void setAdresse1Utilisateur(String adresse1Utilisateur) {
        this.adresse1Utilisateur = adresse1Utilisateur;
    }

    public String getAdresse2Utilisateur() {
        return adresse2Utilisateur;
    }

    public void setAdresse2Utilisateur(String adresse2Utilisateur) {
        this.adresse2Utilisateur = adresse2Utilisateur;
    }

    public String getCpUtilisateur() {
        return cpUtilisateur;
    }

    public void setCpUtilisateur(String cpUtilisateur) {
        this.cpUtilisateur = cpUtilisateur;
    }

    public String getVilleUtilisateur() {
        return villeUtilisateur;
    }

    public void setVilleUtilisateur(String villeUtilisateur) {
        this.villeUtilisateur = villeUtilisateur;
    }

    public Boolean getPresenceAnnuaire() {
        return presenceAnnuaire;
    }

    public void setPresenceAnnuaire(Boolean presenceAnnuaire) {
        this.presenceAnnuaire = presenceAnnuaire;
    }

    public ClassementVue getClassement() {
        return classement;
    }

    public void setClassement(ClassementVue classement) {
        this.classement = classement;
    }

    public Date getDateDerniereConnexionUtilisateur() {
        return dateDerniereConnexionUtilisateur;
    }

    public void setDateDerniereConnexionUtilisateur(Date dateDerniereConnexionUtilisateur) {
        this.dateDerniereConnexionUtilisateur = dateDerniereConnexionUtilisateur;
    }

    public Date getDateActivationConnexionUtilisateur() {
        return dateActivationConnexionUtilisateur;
    }

    public void setDateActivationConnexionUtilisateur(Date dateActivationConnexionUtilisateur) {
        this.dateActivationConnexionUtilisateur = dateActivationConnexionUtilisateur;
    }

    public Boolean getEstActiveUtilisateur() {
        return estActiveUtilisateur;
    }

    public void setEstActiveUtilisateur(Boolean estActiveUtilisateur) {
        this.estActiveUtilisateur = estActiveUtilisateur;
    }

    @Override
    public boolean equals(Object obj){
        if(!(obj instanceof UtilisateurVue)){
            return false;
        }

        UtilisateurVue utilisateur = (UtilisateurVue) obj;
        return this.idUtilisateur.equals(utilisateur.getIdUtilisateur())
                && this.nomUtilisateur.equals(utilisateur.getNomUtilisateur())
                && this.prenomUtilisateur.equals(utilisateur.getPrenomUtilisateur())
                && this.loginUtilisateur.equals(utilisateur.getLoginUtilisateur())
                && this.emailUtilisateur.equals(utilisateur.getEmailUtilisateur())
                && this.role.getLibelleRole().equals(utilisateur.getRole().getLibelleRole());
    }

}
