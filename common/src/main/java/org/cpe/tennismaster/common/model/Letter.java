package org.cpe.tennismaster.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Letter<T> implements Serializable {

    private String id = UUID.randomUUID().toString();

    private Intent intentAction;

    private LienService toQueue;

    private ResponseView<T> response;

    public Letter(Intent intent, LienService lienService, ResponseView<T> response){
        this.intentAction = intent;
        this.toQueue = lienService;
        this.response = response;
    }

}