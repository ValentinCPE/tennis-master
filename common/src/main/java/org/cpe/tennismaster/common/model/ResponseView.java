package org.cpe.tennismaster.common.model;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ResponseView<T> implements Serializable {

    private int codeHttp = HttpStatus.OK.value();

    private String message = HttpStatus.OK.toString();

    private T data;

    public ResponseView(T data){
        this.data = data;
    }

}
