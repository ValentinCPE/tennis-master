package org.cpe.tennismaster.common;

import org.cpe.tennismaster.common.configuration.RestTemplateErrorHandler;
import org.cpe.tennismaster.common.configuration.WebMvcConfig;
import org.cpe.tennismaster.common.configuration.SimpleCorsFilter;
import org.cpe.tennismaster.common.exception.GeneralExceptionHandler;
import org.cpe.tennismaster.common.utils.UtilsTools;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.cpe.tennismaster.common.configuration.activeMQ.*;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@EnableSwagger2
@Import({ActiveMQConfiguration.class, GeneralExceptionHandler.class, WebMvcConfig.class, SimpleCorsFilter.class, UtilsTools.class})
@Component
@Configuration
public class CommonGeneralApplication {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public RestTemplate restTemplate(){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new RestTemplateErrorHandler());
        return restTemplate;
    }

    @Primary
    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }


    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("Europe/Paris"));
    }

}
