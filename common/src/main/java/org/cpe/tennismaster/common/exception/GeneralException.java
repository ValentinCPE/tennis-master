package org.cpe.tennismaster.common.exception;

import lombok.Data;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@Data
@ToString
public abstract class GeneralException extends RuntimeException {

    private int codeHttp;

    private String message;

    public GeneralException(){
        this.codeHttp = this.defineStatusHttp().value();
        this.message = this.defineErrorMessage("");
    }

    public GeneralException(String variable){
        this.codeHttp = this.defineStatusHttp().value();
        this.message = this.defineErrorMessage(variable);
    }

    protected abstract HttpStatus defineStatusHttp();

    protected abstract String defineErrorMessage(String variable);

}
