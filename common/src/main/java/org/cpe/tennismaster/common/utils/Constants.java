package org.cpe.tennismaster.common.utils;

public final class Constants {

    public static final String AUTHORIZATION_HEADER = "Authorization";

    public static final String BEARER = "Bearer ";

    public static final String ADMIN = "admin";

    public static final String USER = "user";

    public static final String NEW = "new";

    public static final String SECRET_KEY = "Kq021mWu42QGx9o82zyM";

    public static final int VALIDITY_SECONDS = 60 * 60 * 12;

    public static final int REMEMBERME_VALIDITY_SECONDS = 24 * 60 * 60; //24 hours

    // Load the TokenProvider configuration[secretKey,tokenValidity] and REMEMBERME_VALIDITY_SECONDS from config
}
